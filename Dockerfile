#
# Builder stage.
# This state compile our TypeScript to get the JavaScript code
#
FROM node:20-alpine AS builder

WORKDIR /

COPY package*.json ./
COPY tsconfig*.json ./
COPY esbuild.config.mjs ./
COPY ./src ./src
RUN npm ci --quiet && npm run build

#
# Build Production stage.
# This state compile get back the JavaScript code from builder stage
# It will also install the production package only
#
FROM node:20-alpine as production-builder

WORKDIR /
ENV NODE_ENV=production

COPY package*.json ./
RUN npm ci --quiet --only=production

FROM node:20-alpine as production

WORKDIR /
ENV NODE_ENV=production

COPY package*.json ./
## We just need the dist folder and node_modules to execute the command
COPY --from=builder ./dist ./dist
COPY --from=production-builder ./node_modules ./node_modules
RUN npm install -g npm

# Add dumb-init to ensure processes run on PID 1. This makes sure SIGTERM signals are delivered correctly
RUN wget -O /usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64
RUN chmod +x /usr/bin/dumb-init
ENTRYPOINT ["/usr/bin/dumb-init", "--"]

## Start
CMD ["npm", "run", "start"]