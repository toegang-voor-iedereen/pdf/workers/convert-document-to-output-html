import packageJson from './package.json' assert { type: 'json' }

import { build } from 'esbuild'

import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

async function main() {
    await build({
        bundle: true,
        sourcemap: true,
        platform: 'node',
        outfile: path.join(__dirname, 'dist/index.mjs'),
        entryPoints: [path.join(__dirname, 'src/index.mts')],
        treeShaking: true,
        external: Object.keys(packageJson.dependencies),
        format: 'esm',
        plugins: [
            {
                name: 'json-loader',
                setup: (compiler) => {
                    compiler.onLoad({ filter: /.json$/ }, async (args) => {
                        const content = await fs.promises.readFile(args.path, 'utf-8')
                        return {
                            contents: `export default JSON.parse(${JSON.stringify(
                                JSON.stringify(JSON.parse(content))
                            )})`,
                        }
                    })
                },
            },
        ],
    })
}
main().catch((err) => {
    console.error(err)
    process.exit(1)
})
