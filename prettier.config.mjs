// @ts-check
import baseConfig from '@toegang-voor-iedereen/configs/prettier.config.mjs'

const prettierConfig = {
    ...baseConfig,
}
export default prettierConfig
