import { vi, it, expect, describe } from 'vitest'
import { type WorkerMessageHandlerInput } from '@toegang-voor-iedereen/kimi-baseworker'
import { Client } from 'minio'
import { handler } from './handler.mjs'
import { logger } from '@toegang-voor-iedereen/typescript-logger'
import { type AttributeValuesReport, ContentClassificationEnum } from './types/Content.js'

describe('handler', () => {
    const content: AttributeValuesReport = {
        bestJobId: 'some-job-id',
        expectedValues: 1,
        isComplete: true,
        values: [
            {
                traceId: 'some-trace-id',
                recordId: 'some-record-id',
                jobId: 'some-job-id',
                timestamp: new Date('2024-01-01').toISOString(),
                success: true,
                confidence: 100,
                contentResult: [
                    {
                        classification: ContentClassificationEnum.Document,
                        bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                        attributes: {},
                        confidence: 100,
                        children: [
                            {
                                classification: ContentClassificationEnum.Page,
                                bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                confidence: 100,
                                attributes: {
                                    pageNumber: 1,
                                },
                                children: [
                                    {
                                        classification: ContentClassificationEnum.Paragraph,
                                        bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                        confidence: 100,
                                        attributes: {},
                                        children: [
                                            {
                                                classification: ContentClassificationEnum.Line,
                                                bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                                confidence: 100,
                                                attributes: {},
                                                children: [
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 0,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'This',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 2,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'is',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 4,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'page',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 6,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'one.',
                                                        },
                                                        children: [],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                classification: ContentClassificationEnum.Page,
                                bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                attributes: {
                                    pageNumber: 2,
                                },
                                confidence: 100,
                                children: [
                                    {
                                        classification: ContentClassificationEnum.Paragraph,
                                        bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                        confidence: 100,
                                        attributes: {},
                                        children: [
                                            {
                                                classification: ContentClassificationEnum.Line,
                                                bbox: { top: 0, left: 0, bottom: 0, right: 0 },
                                                confidence: 100,
                                                attributes: {},
                                                children: [
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 0,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'This',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 2,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'is',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 4,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'page',
                                                        },
                                                        children: [],
                                                    },
                                                    {
                                                        classification:
                                                            ContentClassificationEnum.Word,
                                                        bbox: {
                                                            top: 6,
                                                            left: 0,
                                                            bottom: 0,
                                                            right: 0,
                                                        },
                                                        confidence: 100,
                                                        attributes: {
                                                            text: 'two.',
                                                        },
                                                        children: [],
                                                    },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    }
    const job: WorkerMessageHandlerInput = {
        logger,
        job: {
            recordId: 'document|||mocked-document-id',
            bucketName: 'some-bucket',
            filename: 'some-file.pdf',
            attributes: {
                content,
            },
        },
        jobId: 'some-job-id',
    }
    it('should compose page contents into an html file', async () => {
        const putObject = vi.spyOn(Client.prototype, 'putObject').mockImplementation(async () => {
            return Promise.resolve({
                etag: 'some-etag',
                versionId: 'some-version-id',
            })
        })
        await expect(handler(job)).resolves.toEqual({
            confidence: 100,
            fileResult: {
                bucketName: 'staticassets',
                bucketPath: 'mocked-document-id/index.html',
                fileExtension: 'html',
            },
            success: true,
        })

        expect(putObject).toHaveBeenCalledWith(
            'staticassets',
            'mocked-document-id/index.html',
            expect.any(String),
            { 'Content-Type': 'text/html' }
        )

        expect(putObject.mock.calls[0][2]).toMatchSnapshot()
    })
})
