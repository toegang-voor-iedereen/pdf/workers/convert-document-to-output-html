export const MINIO_STATIC_ASSETS_BUCKET_NAME =
    process.env.MINIO_STATIC_ASSETS_BUCKET_NAME ?? 'staticassets'
