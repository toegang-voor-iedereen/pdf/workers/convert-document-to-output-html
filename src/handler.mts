import {
    type FileWorkerHandlerResult,
    type WorkerMessageHandler,
} from '@toegang-voor-iedereen/kimi-baseworker'
import { Client as MinioClient } from 'minio'
import {
    AttributeValuesReport,
    ContentClassificationEnum,
    type ContentDefinition,
} from './types/Content.js'
import { load } from 'cheerio'
import { elementToHtml } from './contentDefinitionToHtml.mjs'
import { MINIO_STATIC_ASSETS_BUCKET_NAME } from './constants.mjs'

const MINIO_HOST = process.env.MINIO_HOST ?? 'localhost'
const MINIO_PORT = parseInt(process.env.MINIO_PORT ?? '9000')
const MINIO_ACCESS_KEY = process.env.MINIO_ACCESS_KEY ?? 'test'
const MINIO_SECRET_KEY = process.env.MINIO_SECRET_KEY ?? 'test'
const MINIO_OUTPUT_BUCKET_NAME = process.env.MINIO_OUTPUT_BUCKET_NAME ?? 'output'
const MINIO_USE_SSL: boolean = JSON.parse(process.env.MINIO_USE_SSL ?? 'false') === true

const minioClient = new MinioClient({
    endPoint: MINIO_HOST,
    port: MINIO_PORT,
    useSSL: MINIO_USE_SSL,
    accessKey: MINIO_ACCESS_KEY,
    secretKey: MINIO_SECRET_KEY,
})

export const handler: WorkerMessageHandler = async ({
    job,
    jobId,
    logger,
}): Promise<FileWorkerHandlerResult> => {
    const content: unknown = job.attributes.content

    const parsedContent = AttributeValuesReport.safeParse(content)

    if (!parsedContent.success) {
        logger.error(parsedContent.error)
        return {
            success: false,
            error: {
                code: 69,
                message: 'Could not parse attribute content',
            },
            fileResult: null,
            confidence: 0,
        }
    }

    if (!parsedContent.data.bestJobId) {
        return {
            success: false,
            error: { code: 69, message: 'No best job id defined in values report' },
            fileResult: null,
            confidence: 0,
        }
    }

    const bestContent = parsedContent.data.values.find(
        (contentResult) => contentResult.jobId === parsedContent.data.bestJobId
    )

    const document = bestContent?.contentResult.find(
        (element) => element.classification === ContentClassificationEnum.Document
    )

    let pages = (document?.children ?? []).filter(
        (element) => element.classification === ContentClassificationEnum.Page
    )

    pages = Object.values(
        pages.reduce((acc: Record<string, ContentDefinition>, item) => {
            const pageNumber = item.attributes.pageNumber.toString()
            if (pageNumber in acc) {
                acc[pageNumber].children = [
                    ...(acc[pageNumber].children ?? []),
                    ...(item.children ?? []),
                ]
                acc[pageNumber].attributes = { ...acc[pageNumber].attributes, ...item.attributes }
                acc[pageNumber].labels = [...(acc[pageNumber].labels ?? []), ...(item.labels ?? [])]
            } else {
                acc[pageNumber] = item
            }

            return acc
        }, {})
    )

    let text = ''

    // Labels that should be filtered
    const unwanted_labels = ['page_footer', 'page_header']

    pages
        .sort((a, b) => (a.attributes.pageNumber < b.attributes.pageNumber ? -1 : 1))
        .forEach((page) => {
            page.children
                ?.filter(
                    (element) =>
                        !element.labels?.some((label) => unwanted_labels.includes(label.name))
                )
                .sort(
                    (a, b) =>
                        // Sort by distance from the top left corner
                        Math.hypot(a.bbox.left, a.bbox.top) - Math.hypot(b.bbox.left, b.bbox.top)
                )
                .forEach((element) => (text += elementToHtml(element, page)))
        })

    const html = `<!DOCTYPE html>
    <html>
       <head>
          <meta charset="UTF-8" />
          <style>
             html {
             line-height: 1.15;
             -webkit-text-size-adjust: 100%;
             }
             body {
             margin: 0;
             }
             main {
             display: block;
             }
             h1 {
             font-size: 2em;
             margin: 0.67em 0;
             }
             hr {
             box-sizing: content-box;
             height: 0;
             overflow: visible;
             }
             pre {
             font-family: monospace,monospace;
             font-size: 1em;
             }
             a {
             background-color: transparent;
             }
             abbr[title] {
             border-bottom: none;
             text-decoration: underline;
             text-decoration: underline dotted;
             }
             b,strong {
             font-weight: bolder;
             }
             code,kbd,samp {
             font-family: monospace,monospace;
             font-size: 1em;
             }
             small {
             font-size: 80%;
             }
             sub,sup {
             font-size: 75%;
             line-height: 0;
             position: relative;
             vertical-align: baseline;
             }
             sub {
             bottom: -0.25em;
             }
             sup {
             top: -0.5em;
             }
             img {
             border-style: none;
             }
             button,input,optgroup,select,textarea {
             font-family: inherit;
             font-size: 100%;
             line-height: 1.15;
             margin: 0;
             }
             button,input {
             overflow: visible;
             }
             button,select {
             text-transform: none;
             }
             button,[type="button"],[type="reset"],[type="submit"] {
             -webkit-appearance: button;
             }
             button::-moz-focus-inner,[type="button"]::-moz-focus-inner,[type="reset"]::-moz-focus-inner,[type="submit"]::-moz-focus-inner {
             border-style: none;
             padding: 0;
             }
             button:-moz-focusring,[type="button"]:-moz-focusring,[type="reset"]:-moz-focusring,[type="submit"]:-moz-focusring {
             outline: 1px dotted ButtonText;
             }
             fieldset {
             padding: 0.35em 0.75em 0.625em;
             }
             legend {
             box-sizing: border-box;
             color: inherit;
             display: table;
             max-width: 100%;
             padding: 0;
             white-space: normal;
             }
             progress {
             vertical-align: baseline;
             }
             textarea {
             overflow: auto;
             }
             [type="checkbox"],[type="radio"] {
             box-sizing: border-box;
             padding: 0;
             }
             [type="number"]::-webkit-inner-spin-button,[type="number"]::-webkit-outer-spin-button {
             height: auto;
             }
             [type="search"] {
             -webkit-appearance: textfield;
             outline-offset: -2px;
             }
             [type="search"]::-webkit-search-decoration {
             -webkit-appearance: none;
             }
             ::-webkit-file-upload-button {
             -webkit-appearance: button;
             font: inherit;
             }
             details {
             display: block;
             }
             summary {
             display: list-item;
             }
             template {
             display: none;
             }
             [hidden] {
             display: none;
             }
             .site-logo img {
             margin-left: 100px;
             }
             .layout-header {
             background-color: white;
             }
             .layout-header .region-header > .grid {
             max-width: 1128px;
             margin: 0 auto;
             display: flex;
             flex-direction: column;
             }
             .layout-header .region-header > .grid > div {
             align-self: center;
             }
             .layout-header__top {
             height: 156px;
             overflow: hidden;
             }
             .layout-header__footer {
             display: flex;
             justify-content: space-between;
             align-items: center;
             height: 56px;
             background-color: #01689b;
             color: white;
             }
             .layout-header__footer a {
             color: white;
             }
             .layout-header__footer a:hover {
             color: rgba(1, 104, 155, 0.2);
             }
             .layout-header__footer .menu-wrapper {
             flex: 0 0 200px;
             }
             .layout-header__footer .branding--text {
             flex: 1 0 auto;
             text-align: center;
             font-weight: 700;
             }
             .layout-header__footer .share {
             flex: 0 0 200px;
             }
             .layout-header__footer .share .button--share {
             margin-left: auto;
             }
             .container {
             max-width: 936px;
             margin: 0 auto;
             }
             .layout-main {
             padding: 96px 0;
             background-color: #f3f3f3;
             }
             .layout-content {
             margin: 0 auto;
             padding: 16px;
             background-color: white;
             }
             @media all and (min-width: 768px) {
             .layout-content.grid {
             box-sizing: border-box;
             display: flex;
             margin-left: 0;
             margin-right: 0;
             padding: 40px;
             }
             }
             @media all and (min-width: 936px) {
             .layout-content.grid {
             padding: 48px 96px;
             }
             }
             @media all and (min-width: 1280px) {
             .layout-content.grid {
             padding: 48px 96px;
             }
             }
             .region-content {
             width: 100%;
             max-width: 100%;
             }
             .main-content {
             padding: 0 16px 32px;
             }
             @media all and (min-width: 936px) {
             .main-content {
             box-sizing: border-box;
             flex: 0 0 66.6666666667%;
             margin-bottom: 16px;
             }
             }
             @media all and (min-width: 1280px) {
             .main-content {
             padding: 0;
             }
             }
             .region-content .block {
             margin-block: 0 32px;
             }
             .region-content .block.block-system-branding-block {
             margin-block: 0;
             }
             .layout-container {
             font-family: 'RijksoverheidSansWebText', sans-serif;
             font-weight: 400;
             color: #333333;
             font-size: 1rem;
             line-height: 1.5em;
             }
             p {
             margin: 0 0 1rem;
             }
             a {
             color: #01689b;
             }
             a:focus,
             a:hover {
             color: black;
             }
             a:visited {
             color: #01689b;
             }
             .paragraph--type--text {
             margin-block: 0 16px;
             }
             p {
             max-width: 936px;
             margin-block: 0 32px;
             font-family: 'RijksoverheidSansWebText', sans-serif;
             }
             p > strong,
             .intro p {
             font-weight: 700;
             }
             h5,
             h4,
             h3,
             h2,
             h1 {
             color: #01689b;
             font-family: 'RijksoverheidSansWebText', sans-serif;
             line-height: 1.5;
             font-weight: 700;
             margin: 0 0 0.5rem;
             padding: 0;
             }
             h1 {
             font-size: 2rem;
             }
             h2 {
             font-size: 1.5rem;
             }
             h3 {
             font-size: 1.25rem;
             }
             h4 {
             font-size: 1rem;
             }
             h5 {
             font-size: 1rem;
             font-weight: 400;
             }
             @media all and (min-width: 768px) {
             h1 {
             font-size: 2.5rem;
             }
             h2 {
             font-size: 2rem;
             }
             h3 {
             font-size: 1.5rem;
             }
             h4 {
             font-size: 1rem;
             }
             h5 {
             font-size: 1rem;
             font-weight: 300;
             }
             }
             .menu--main {
             position: fixed;
             z-index: 10;
             inset-block-start: 0;
             inset-inline-start: 100%;
             visibility: hidden;
             overflow: auto;
             flex-basis: max-content;
             width: 100%;
             max-width: 100%;
             height: 100%;
             padding: 16px;
             }
             .menu--main.is-active {
             visibility: visible;
             transform: translateX(-100%);
             }
             .menu ul.menu {
             list-style: none;
             margin: 0;
             padding: 0;
             }
             .page-title {
             color: #01689b;
             }
             .form-item {
             margin-block: 16px;
             }
             .form-item__label {
             display: block;
             margin-block: 0 5.3333333333px;
             font-size: 16px;
             line-height: 1.5;
             font-weight: 700;
             font-family: 'RijksoverheidSansWebText', sans-serif;
             }
             .form-item .description {
             font-size: 14px;
             line-height: 1.2;
             }
             .form-element {
             font-size: 16px;
             line-height: 1.5;
             font-weight: 400;
             font-family: 'RijksoverheidSansWebText', sans-serif;
             }
             [type='color'],
             [type='date'],
             [type='datetime-local'],
             [type='email'],
             [type='file'],
             [type='month'],
             [type='number'],
             [type='password'],
             [type='search'],
             [type='tel'],
             [type='text'],
             [type='time'],
             [type='url'],
             [type='week'],
             textarea {
             width: 100%;
             max-width: 100%;
             margin-block: 0 16px;
             padding: 0 16px;
             border: 1px solid #cccccc;
             border-radius: 4px;
             font-family: 'RijksoverheidSansWebText', sans-serif;
             font-size: 1rem;
             -webkit-appearance: none;
             appearance: none;
             }
             [type='file'] {
             height: auto;
             width: inherit;
             padding-block: 16px;
             margin-block: 0 8px;
             }
             .pt-doccess-initial-content-file-upload-form > form {
             background-color: #ffffff;
             padding: 32px;
             display: flex;
             flex-direction: column;
             align-items: center;
             border: 1px dashed #cccccc;
             }
             .content-moderation-entity-moderation-form {
             font-family: 'RijksoverheidSansWebText', sans-serif;
             font-size: 0.9rem;
             }
             .form-element--type-select {
             padding: 8px 48px 8px 16px;
             background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 14 9'%3e%3cpath fill='none' stroke-width='1.5' d='M1 1l6 6 6-6' stroke='%23545560'/%3e%3c/svg%3e");
             background-repeat: no-repeat;
             background-position: 100% 50%;
             background-size: 2.75rem 0.5625rem;
             background-color: #ffffff;
             -webkit-appearance: none;
             appearance: none;
             border: 1px solid #cccccc;
             border-radius: 0;
             }
             .button {
             display: flex;
             align-items: center;
             border: none;
             padding: 1rem 2rem;
             margin: 0;
             text-decoration: none;
             background: transparent;
             font-size: 1rem;
             text-align: center;
             -webkit-appearance: none;
             -moz-appearance: none;
             }
             input[type='submit'],
             input.button {
             background-color: #01689b;
             color: white;
             padding: 8px 16px;
             cursor: pointer;
             }
             input[type='submit']:hover,
             input.button:hover {
             background-color: #015078;
             }
             .button--mobile-nav {
             background-color: initial;
             }
             .button--mobile-nav .mobile-nav__icon {
             cursor: pointer;
             }
             .button--mobile-nav .mobile-nav__label {
             color: white;
             margin-left: 16px;
             font-weight: 700;
             }
             .button--share {
             background-color: initial;
             }
             .button--share .share__icon {
             cursor: pointer;
             }
             .button--share .share__label {
             color: white;
             margin-right: 16px;
             font-weight: 700;
             }
             main .paragraph--type--table {
             max-width: 100%;
             overflow-x: auto;
             }
             main table,
             main table.views-table {
             width: 936px;
             max-width: 100%;
             margin-block: 0 48px;
             border-collapse: collapse;
             }
             main table th,
             main table td,
             main table.views-table th,
             main table.views-table td {
             margin-block: 0;
             margin-inline-start: 0;
             margin-inline-end: 0;
             vertical-align: top;
             text-align: start;
             padding: 16px;
             border: 1px solid #cccccc;
             border-collapse: collapse;
             }
             main table th,
             main table.views-table th {
             font-weight: 700;
             color: #01689b;
             background-color: #d9e8f0;
             border-block-end: 2px solid #01689b;
             }
             main ul,
             main ol,
             main dl {
             font-family: 'RijksoverheidSansWebText', sans-serif;
             font-size: 16px;
             line-height: 1.5;
             max-width: 936px;
             margin-block: 0 32px;
             padding: 0 0 0 24px;
             }
             main ul li,
             main ol li,
             main dl li {
             margin: 0;
             padding: 0 0 0 3px;
             }
             main ul ul,
             main ul ol,
             main ul dl,
             main ol ul,
             main ol ol,
             main ol dl,
             main dl ul,
             main dl ol,
             main dl dl {
             margin-block: 0;
             padding: 0;
             }
             main dl {
             margin-block: 0;
             padding: 0;
             }
             main dl > dt {
             font-weight: 700;
             margin: 0;
             padding: 0;
             }
             main dl > dd {
             margin: 0;
             padding: 0;
             }
             img {
             max-width: 100%;
             height: auto;
             }
             blockquote {
             font-family: 'RijksoverheidSansWebText', sans-serif;
             font-size: 1.5rem;
             font-style: italic;
             color: #01689b;
             margin: 0 0 16px;
             padding: 16px 16px 16px 1.5rem;
             border-left: 2px solid #01689b;
             }
             blockquote > p {
             font-size: 1.5rem;
             margin: 0;
             }
          </style>
       </head>
       <body>
          <div class="layout-container corporate-identity-default">
             <header role="banner" class="layout-header">
                <div class="layout-header__top">
                   <div class="region region-header">
                      <div class="grid">
                         <div class="block block-system block-system-branding-block">
                            <a href="/" rel="home" class="site-logo">
                            <img src="data:image/svg+xml;base64,PHN2ZwogICAgICAgICAgICAgICAgICAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgICAgICAgICAgICAgICAgICB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIKICAgICAgICAgICAgICAgICAgICB3aWR0aD0iMTQ2cHgiCiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PSIxMDBweCIKICAgICAgICAgICAgICAgICAgICB2aWV3Qm94PSIwIDAgMTQ2IDEwMCIKICAgICAgICAgICAgICAgICAgICB2ZXJzaW9uPSIxLjEiCiAgICAgICAgICAgICAgICAgID4KICAgICAgICAgICAgICAgICAgICA8dGl0bGU+R3JvdXAgMjwvdGl0bGU+CiAgICAgICAgICAgICAgICAgICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICAgICAgICAgICAgICAgICAgPGRlZnMgLz4KICAgICAgICAgICAgICAgICAgICA8ZwogICAgICAgICAgICAgICAgICAgICAgaWQ9IlBhZ2UtMSIKICAgICAgICAgICAgICAgICAgICAgIHN0cm9rZT0ibm9uZSIKICAgICAgICAgICAgICAgICAgICAgIHN0cm9rZS13aWR0aD0iMSIKICAgICAgICAgICAgICAgICAgICAgIGZpbGw9Im5vbmUiCiAgICAgICAgICAgICAgICAgICAgICBmaWxsLXJ1bGU9ImV2ZW5vZGQiCiAgICAgICAgICAgICAgICAgICAgPgogICAgICAgICAgICAgICAgICAgICAgPGcKICAgICAgICAgICAgICAgICAgICAgICAgaWQ9Ikdyb3VwLTIiCiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMzMuMDAwMDAwLCAwLjAwMDAwMCkiCiAgICAgICAgICAgICAgICAgICAgICA+CiAgICAgICAgICAgICAgICAgICAgICAgIDxwb2x5Z29uCiAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlJlY3RhbmdsZSIKICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxsPSIjRDhEOEQ4IgogICAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk9IjAiCiAgICAgICAgICAgICAgICAgICAgICAgICAgcG9pbnRzPSIwIDAgMzE0IDAgMzE0IDEyNSAwIDEyNSIKICAgICAgICAgICAgICAgICAgICAgICAgLz4KICAgICAgICAgICAgICAgICAgICAgICAgPGcKICAgICAgICAgICAgICAgICAgICAgICAgICBpZD0ibG9nby1kZWZhdWx0IgogICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEzMy4wMDAwMDAsIDAuMDAwMDAwKSIKICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxsLXJ1bGU9Im5vbnplcm8iCiAgICAgICAgICAgICAgICAgICAgICAgID4KICAgICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbgogICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlNoYXBlIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iIzE1NDI3MyIKICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvaW50cz0iMCAwIDUwIDAgNTAgMTAwIDAgMTAwIgogICAgICAgICAgICAgICAgICAgICAgICAgIC8+CiAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGgKICAgICAgICAgICAgICAgICAgICAgICAgICAgIGQ9Ik0yNS45MDQsNzcuMzYzIEwyNS4wMzQsNzcuMzYzIEwyNS4wMzQsNzQuOTUgTDI1LjkwNCw3NC45NSBMMjUuOTA0LDc3LjM2MyBaIE0yOS44MjgsNjYuNzI1IEwyOC45NTgsNjYuNzI1IEwyOC45NTgsNjQuMzEgTDI5LjgyOCw2NC4zMSBMMjkuODI4LDY2LjcyNCBMMjkuODI4LDY2LjcyNSBaIE0xMy43MTMsNjIuNTYyIEMxMy42NDcsNjIuNTYyIDEzLjE5Nyw2Mi41NjMgMTMuMTk3LDYyLjY1NiBDMTMuMTk3LDYyLjc0OSAxMy40MDcsNjIuNzAxIDEzLjcxMyw2Mi43NTMgQzE0LjQwOSw2Mi44NzIgMTQuNjg0LDYzLjIyMyAxNS4xNzMsNjMuMjIzIEMxNS42MTcsNjMuMjIzIDE2LjA3LDYyLjkyMiAxNS44OTMsNjIuMjggQzE1Ljg2NCw2Mi4xNzMgMTUuODAxLDYyLjE5OCAxNS43ODcsNjIuMjcxIEMxNS43MTksNjIuNTU3IDE1LjM2Nyw2Mi43MjIgMTQuOTk2LDYyLjcyMiBDMTQuNTM2LDYyLjcyMiAxNC40MzgsNjIuNTYzIDEzLjcxMyw2Mi41NjMgTDEzLjcxMyw2Mi41NjIgWiBNOC45Miw2NC45NzMgQzguOTk2LDY1LjA3OSA5LjgxNyw2Ni4xNDQgOC41NTcsNjYuNTUyIEM4LjUwMSw2Ni41NzEgOC40ODIsNjYuNjI1IDguNTY2LDY2LjY2MSBDOS40OTcsNjcuMDUgOS45NDgsNjYuMzM1IDkuOTQ4LDY2LjMzNSBDMTAuNDE2LDY3LjQwNyA5LjY5OCw2OC4wMDEgOS42OTgsNjguMDAxIEM5Ljc5OSw2OC4xOTUgMTAuMjQ0LDY4LjUxNyAxMC45Miw2OC42NjggQzExLjE3NSw2OC41MzEgMTEuNTM1LDY4LjI2OSAxMS42ODUsNjcuODY2IEMxMS42ODUsNjcuODY2IDExLjg2Nyw2OC4zMzQgMTEuNzAyLDY4Ljc4MSBDMTMuNjA0LDY4Ljk1NCAxNC4yNzcsNjcuNDMzIDE1Ljc0Miw2Ny43MDcgQzE1Ljc4NSw2Ny43MTUgMTUuODA0LDY3LjY4OCAxNS44MDQsNjcuNjQzIEMxNS44MDQsNjYuNzA1IDE1LjIzLDY0Ljk5MSAxMy40MTYsNjQuNjQ0IEMxMS42MDcsNjQuMjk1IDEyLjA5NCw2My41NTIgMTIuMDk0LDYzLjU1MiBDMTIuODU3LDYzLjUxNyAxMy4yOTcsNjMuODQ5IDEzLjI5Nyw2My44NDkgQzEzLjQzNiw2My42MjEgMTMuMjQsNjMuMzc0IDEzLjI0LDYzLjM3NCBDMTMuMzEzLDYzLjI4NiAxMy4zNTMsNjIuOTc2IDEzLjM1Myw2Mi45NzYgQzEyLjQ5NCw2Mi45NzUgMTIuMjY5LDYyLjc5IDEyLjI2OSw2Mi43OSBDMTIuMTM3LDYyLjI4MSAxMi40OTEsNjEuNTYgMTMuNzY3LDYyLjMzMSBDMTQuMDg1LDYyLjA0NyAxNC4wNjEsNjEuNjE1IDE0LjA2MSw2MS42MTUgQzE0LjE1Nyw2MS41NyAxNC4yMTcsNjEuNDE5IDE0LjIxNyw2MS4zNTQgQzE0LjIxNyw2MS4xNDIgMTMuMTk3LDYwLjU3IDEyLjkzNSw2MC41MDIgQzEyLjg3MSw2MC4xOTMgMTIuNDc2LDU5Ljc0NyAxMS4yNzcsNTkuOTA4IEMxMC43MSw1OS45NzggMTAuMzAxLDU5LjgzIDEwLjQ5Miw1OS40OTkgQzEwLjUyNCw1OS40NDMgMTAuNDIzLDU5LjQzMyAxMC4zNyw1OS40ODcgQzEwLjIzOSw1OS42MTggMTAuMDU4LDU5Ljk4OCAxMC41NTQsNjAuMjM4IEMxMS4xMDQsNjAuNTE2IDExLjEzOCw2MS4wNjggMTEuMTQyLDYxLjE0OCBDMTEuMTUxLDYxLjI2NSAxMS4wMTEsNjEuMjc1IDEwLjk4LDYxLjE1OSBDMTAuNzI3LDYwLjIyOCA5LjQ1NCw2MC4wMjggOC43MDEsNjAuNTIyIEM4Ljc0NSw2MS4wMjEgOS4yODEsNjEuMzUgOS41NzUsNjEuMzUgQzkuNzQ0LDYxLjM1IDkuOTc4LDYxLjE4MiA5Ljk3OCw2MC45NDkgQzkuOTc4LDYwLjgzNiAxMC4wMzYsNjAuODUxIDEwLjA2OSw2MC45MzggQzEwLjE5NCw2MS4yNyA5LjcwMyw2MS45NTQgOC45NzUsNjEuNTM4IEM4LjIzNCw2Mi4yOCA4LjUzLDYzLjE5MyA4LjYwNCw2My41MyBDOC43MzcsNjQuMTM1IDguNDQxLDY0LjcyMSA3Ljg5Niw2NC40NDggQzcuODY4LDY0LjQzNSA3Ljg0LDY0LjQyNCA3Ljg0LDY0LjQ4OSBDNy44NCw2NC44NyA4LjMzNiw2NS4zIDguOTIxLDY0Ljk3MyBMOC45Miw2NC45NzMgWiBNMTMuMDAzLDYxLjIzNSBDMTIuNjU3LDYxLjYyMSAxMi4wOTMsNjEuNTMyIDExLjk1OCw2MC44OTUgQzEyLjY2OSw2MC44NTggMTIuOTc1LDYwLjkzIDEzLjAwMyw2MS4yMzUgWiBNMzUuMDcyLDYyLjcyMSBDMzQuNyw2Mi43MjEgMzQuMzQ5LDYyLjU1NyAzNC4yODEsNjIuMjcgQzM0LjI2Niw2Mi4xOTcgMzQuMjAzLDYyLjE3MiAzNC4xNzUsNjIuMjc5IEMzMy45OTksNjIuOTIxIDM0LjQ1Miw2My4yMjIgMzQuODk1LDYzLjIyMiBDMzUuMzg0LDYzLjIyMiAzNS42NTgsNjIuODcyIDM2LjM1NCw2Mi43NTIgQzM2LjY2LDYyLjcgMzYuODcsNjIuNzQ4IDM2Ljg3LDYyLjY1NSBDMzYuODcsNjIuNTYyIDM2LjQyLDYyLjU2MSAzNi4zNTQsNjIuNTYxIEMzNS42Myw2Mi41NjEgMzUuNTMxLDYyLjcyIDM1LjA3MSw2Mi43MiBMMzUuMDcyLDYyLjcyMSBaIE00Mi4yMjgsNjQuNDg5IEM0Mi4yMjgsNjQuNDI0IDQyLjIsNjQuNDM1IDQyLjE3Myw2NC40NDggQzQxLjYyOCw2NC43MjEgNDEuMzMyLDY0LjEzNSA0MS40NjUsNjMuNTMgQzQxLjUzOSw2My4xOTMgNDEuODM1LDYyLjI3OSA0MS4wOTQsNjEuNTM4IEM0MC4zNjUsNjEuOTU0IDM5Ljg3NSw2MS4yNyA0MCw2MC45MzggQzQwLjAzNCw2MC44NSA0MC4wOTEsNjAuODM2IDQwLjA5MSw2MC45NDkgQzQwLjA5MSw2MS4xODEgNDAuMzI1LDYxLjM1IDQwLjQ5NCw2MS4zNSBDNDAuNzg4LDYxLjM1IDQxLjMyNCw2MS4wMjEgNDEuMzY5LDYwLjUyMiBDNDAuNjE2LDYwLjAyNyAzOS4zNDMsNjAuMjI4IDM5LjA5LDYxLjE1OSBDMzkuMDU4LDYxLjI3NiAzOC45MTcsNjEuMjY1IDM4LjkyNyw2MS4xNDggQzM4LjkzMiw2MS4wNjggMzguOTY1LDYwLjUxNyAzOS41MTUsNjAuMjM4IEM0MC4wMTEsNTkuOTg5IDM5LjgzMSw1OS42MTggMzkuNjk5LDU5LjQ4NyBDMzkuNjQ1LDU5LjQzMyAzOS41NDQsNTkuNDQzIDM5LjU3Niw1OS40OTkgQzM5Ljc2OCw1OS44MjkgMzkuMzU5LDU5Ljk3OCAzOC43OTEsNTkuOTA4IEMzNy41OTIsNTkuNzQ3IDM3LjE5Niw2MC4xOTMgMzcuMTMzLDYwLjUwMiBDMzYuODcsNjAuNTcgMzUuODUxLDYxLjE0MSAzNS44NTEsNjEuMzU0IEMzNS44NjA1MTkxLDYxLjQ2MDIyMzcgMzUuOTE3OTUwOSw2MS41NTYzMTE0IDM2LjAwNyw2MS42MTUgQzM2LjAwNyw2MS42MTUgMzUuOTgzLDYyLjA0NyAzNi4zMDEsNjIuMzMxIEMzNy41NzYsNjEuNTYgMzcuOTMsNjIuMjgxIDM3Ljc5OSw2Mi43OSBDMzcuNzk5LDYyLjc5IDM3LjU3NCw2Mi45NzQgMzYuNzE0LDYyLjk3NiBDMzYuNzE0LDYyLjk3NiAzNi43NTUsNjMuMjg2IDM2LjgyOCw2My4zNzQgQzM2LjgyOCw2My4zNzQgMzYuNjMyLDYzLjYyMSAzNi43NzEsNjMuODQ5IEMzNi43NzEsNjMuODQ5IDM3LjIxMSw2My41MTcgMzcuOTc0LDYzLjU1MiBDMzcuOTc0LDYzLjU1MiAzOC40NjEsNjQuMjk1IDM2LjY1Miw2NC42NDQgQzM0Ljg0Myw2NC45OTMgMzQuMjY0LDY2LjcwNSAzNC4yNjQsNjcuNjQzIEMzNC4yNjQsNjcuNjg4IDM0LjI4Myw2Ny43MTUgMzQuMzI2LDY3LjcwNyBDMzUuNzkyLDY3LjQzNCAzNi40NjQsNjguOTU0IDM4LjM2Niw2OC43ODEgQzM4LjIsNjguMzM0IDM4LjM4Myw2Ny44NjYgMzguMzgzLDY3Ljg2NiBDMzguNTMzLDY4LjI2OSAzOC44OTIsNjguNTMyIDM5LjE0OCw2OC42NjggQzM5LjgyMyw2OC41MTcgNDAuMjY4LDY4LjE5NCA0MC4zNyw2OC4wMDEgQzQwLjM3LDY4LjAwMSAzOS42NTIsNjcuNDA3IDQwLjEyMSw2Ni4zMzUgQzQwLjEyMSw2Ni4zMzUgNDAuNTcyLDY3LjA1IDQxLjUwMyw2Ni42NjEgQzQxLjU4Nyw2Ni42MjUgNDEuNTY3LDY2LjU3MSA0MS41MTIsNjYuNTUyIEM0MC4yNTEsNjYuMTQ1IDQxLjA3Miw2NS4wOCA0MS4xNDksNjQuOTczIEM0MS43MzQsNjUuMjk5IDQyLjIyOSw2NC44NjkgNDIuMjI5LDY0LjQ4OSBMNDIuMjI4LDY0LjQ4OSBaIE0zOC4xMSw2MC44OTYgQzM3Ljk3NCw2MS41MzMgMzcuNDEsNjEuNjIyIDM3LjA2NSw2MS4yMzYgQzM3LjA5NCw2MC45MzEgMzcuMzk5LDYwLjg1OSAzOC4xMSw2MC44OTYgWiBNMjQuNyw1MC4yMTIgQzI0LjcwMzEzMTIsNTAuMzQ3Nzg0MSAyNC43NDYyNjYyLDUwLjQ3OTYyNDIgMjQuODI0LDUwLjU5MSBDMjQuOTM2LDUwLjc1IDI0LjkxOSw1MC44MzggMjQuODY2LDUwLjg5MSBDMjQuODEzLDUwLjk0NCAyNC43MjUsNTAuOTYxIDI0LjU2Niw1MC44NSBDMjQuNDU1MDEyLDUwLjc3MTM2NTIgMjQuMzIyOTkxOSw1MC43Mjc4MjMgMjQuMTg3LDUwLjcyNSBDMjMuOTEyLDUwLjcyNSAyMy43NzcsNTAuOTA1IDIzLjc3Nyw1MS4wNTkgQzIzLjc3Nyw1MS4yMTMgMjMuOTEzLDUxLjM5MyAyNC4xODcsNTEuMzkzIEMyNC4zMjI5OTE5LDUxLjM5MDE3NyAyNC40NTUwMTIsNTEuMzQ2NjM0OCAyNC41NjYsNTEuMjY4IEMyNC43MjYsNTEuMTU3IDI0LjgxNCw1MS4xNzQgMjQuODY2LDUxLjIyNyBDMjQuOTE4LDUxLjI4IDI0Ljg0MSw1MS44MzEgMjQuNzgsNTIuMjY0IEMyNC4zMjYsNTIuMzU5IDI0LjEwNyw1Mi43MjMgMjQuMTA3LDUyLjkzNSBDMjQuMTA3LDUzLjQ4MSAyNC42OTEsNTMuNzk3IDI1LjAzNSw1My45OCBDMjUuMzc4LDUzLjc5NyAyNS45NjIsNTMuNDgxIDI1Ljk2Miw1Mi45MzUgQzI1Ljk2Miw1Mi43MjMgMjUuNzQzLDUyLjM1OSAyNS4yODksNTIuMjY0IEMyNS4yMjcsNTEuODMgMjUuMTUsNTEuMjc4IDI1LjIwMiw1MS4yMjcgQzI1LjI1NCw1MS4xNzYgMjUuMzQzLDUxLjE1NyAyNS41MDIsNTEuMjY4IEMyNS42MTI5ODgsNTEuMzQ2NjM0OCAyNS43NDUwMDgxLDUxLjM5MDE3NyAyNS44ODEsNTEuMzkzIEMyNi4xNTYsNTEuMzkzIDI2LjI5MSw1MS4yMTMgMjYuMjkxLDUxLjA1OSBDMjYuMjkxLDUwLjkwNSAyNi4xNTUsNTAuNzI1IDI1Ljg4MSw1MC43MjUgQzI1Ljc0NTAwODEsNTAuNzI3ODIzIDI1LjYxMjk4OCw1MC43NzEzNjUyIDI1LjUwMiw1MC44NSBDMjUuMzQyLDUwLjk2MiAyNS4yNTQsNTAuOTQ0IDI1LjIwMiw1MC44OTEgQzI1LjE1LDUwLjgzOCAyNS4xMzIsNTAuNzUgMjUuMjQ0LDUwLjU5MSBDMjUuMjk5LDUwLjUxMyAyNS4zNjksNTAuMzcyIDI1LjM2OSw1MC4yMTIgQzI1LjM2OSw0OS45MzcgMjUuMTg5LDQ5LjgwMSAyNS4wMzUsNDkuODAxIEMyNC44ODEsNDkuODAxIDI0LjcwMSw0OS45MzggMjQuNzAxLDUwLjIxMiBMMjQuNyw1MC4yMTIgWiBNMjUuMDM0LDU0LjI3MiBDMjQuNjY2LDU0LjI3MiAyNC4zNjcsNTQuNTY5IDI0LjM2Nyw1NC45MzggQzI0LjM2Nyw1NS4zMDcgMjQuNjY1LDU1LjYwNCAyNS4wMzQsNTUuNjA0IEMyNS40MDMsNTUuNjA0IDI1LjcsNTUuMzA2IDI1LjcsNTQuOTM4IEMyNS43LDU0LjU3IDI1LjQwMSw1NC4yNzIgMjUuMDM0LDU0LjI3MiBaIE0yNS4wMzQsNjAuNjgzIEMyNy43MDIsNjAuNjgzIDI5LjIwOCw2MS4xOTIgMjkuMjA4LDYxLjE5MiBDMjkuMjIzLDU5Ljg0NCAyOS4yMTMsNTkuMDkyIDI5Ljk4Nyw1OS4yMDUgQzI5LjU2NSw1OC4wNzIgMzEuMzg1LDU3LjYyOCAzMS4zODUsNTYuMTUxIEMzMS4zODUsNTUuMTc5IDMwLjY5Nyw1NS4wNjkgMzAuNDk0LDU1LjA2OSBDMjkuODksNTUuMDY5IDI5Ljk4NSw1NS40NjIgMjkuNDcxLDU1LjQ2MiBDMjkuNDA3LDU1LjQ2MiAyOS4zNDEsNTUuNDQ2IDI5LjM0MSw1NS40OTMgQzI5LjM0MSw1NS42NjMgMjkuNDg2LDU1Ljk1MyAyOS43MjIsNTUuOTUzIEMzMC4wMzgsNTUuOTUzIDMwLjA0NCw1NS42MzcgMzAuMjc5LDU1LjYzNyBDMzAuMzgsNTUuNjM3IDMwLjUyNiw1NS43MTkgMzAuNTI2LDU2LjA0MyBDMzAuNTI2LDU2LjY3IDMwLjExNSw1Ny4zNjcgMjkuNjI5LDU3LjkwNSBDMjkuNTE3LDU3Ljc1MSAyOS4zNjYsNTcuNjU2IDI5LjE4NCw1Ny42NTYgQzI4Ljg5Nyw1Ny42NTYgMjguNjQ1LDU3Ljk1NiAyOC42NDUsNTguMzI2IEMyOC42NDI2ODM3LDU4LjQ0NzIxMzIgMjguNjY3MjczOSw1OC41Njc0MzE5IDI4LjcxNyw1OC42NzggQzI4LjU1NSw1OC43NjggMjguNDExLDU4LjgyIDI4LjMwMyw1OC44MiBDMjguMDg0LDU4LjgyIDI3Ljc1NCw1OC43MzEgMjcuNzU0LDU4LjI1MSBDMjcuNzU0LDU3LjE4MiAyOS4xODgsNTYuMjQgMjkuMTg4LDU0Ljg4NCBDMjkuMTg4LDU0LjIzNyAyOC43NTYsNTMuNTkyIDI3Ljg1Miw1My41OTIgQzI2Ljg4Myw1My41OTIgMjcuMDYzLDU0LjQ3NyAyNi4yMjUsNTQuNDc3IEMyNi4xODcsNTQuNDc3IDI2LjE1Niw1NC40ODkgMjYuMTU2LDU0LjUyMyBDMjYuMTU2LDU0LjU3OSAyNi4zMDEsNTUuMDY4IDI2LjczNSw1NS4wNjggQzI3LjE5OSw1NS4wNjggMjcuMzMsNTQuMjY2IDI3Ljc0LDU0LjI2NiBDMjcuODk0LDU0LjI2NiAyOC4xMjMsNTQuMzU2IDI4LjEyMyw1NC45IEMyOC4xMjMsNTUuMzIzIDI3LjkxMyw1Ni4wMDYgMjcuNjEyLDU2LjY2MiBDMjcuNTAzLDU2LjU1MyAyNy4zNTYsNTYuNDc0IDI3LjE4OSw1Ni40NzQgQzI2LjgzLDU2LjQ3NCAyNi41NzIsNTYuODIxIDI2LjU3Miw1Ny4yMzcgQzI2LjU2ODgyMzQsNTcuNDkzNjE2NSAyNi42NzA3MDU4LDU3Ljc0MDM3NDMgMjYuODU0LDU3LjkyIEMyNi42NDIsNTguMTkxIDI2LjQyNiw1OC40MTEgMjYuMTMzLDU4LjQxMSBDMjUuNTU0LDU4LjQxMSAyNS4yOTgsNTguMDgyIDI1LjI5OCw1Ny43MjQgQzI1LjI5OCw1Ny4zMzcgMjYuMDA1LDU3LjE5NiAyNi4wMDUsNTYuNTkxIEMyNi4wMDUsNTYuMTggMjUuNzM4LDU1Ljk0OCAyNS40OCw1NS45NDggQzI1LjIwMyw1NS45NDggMjUuMDYxLDU2LjEzMSAyNS4wMzQsNTYuMTMxIEMyNS4wMDcsNTYuMTMxIDI0Ljg2NCw1NS45NDggMjQuNTg3LDU1Ljk0OCBDMjQuMzMsNTUuOTQ4IDI0LjA2Miw1Ni4xOCAyNC4wNjIsNTYuNTkxIEMyNC4wNjIsNTcuMTk2IDI0Ljc2OSw1Ny4zMzYgMjQuNzY5LDU3LjcyNCBDMjQuNzY5LDU4LjA4MiAyNC41MTQsNTguNDExIDIzLjkzNSw1OC40MTEgQzIzLjY0MSw1OC40MTEgMjMuNDI1LDU4LjE5MSAyMy4yMTMsNTcuOTIgQzIzLjM5MSw1Ny43NTMgMjMuNDk1LDU3LjUgMjMuNDk1LDU3LjIzNyBDMjMuNDk1LDU2LjgyMiAyMy4yMzcsNTYuNDc0IDIyLjg3OCw1Ni40NzQgQzIyLjcxMiw1Ni40NzQgMjIuNTY0LDU2LjU1NCAyMi40NTYsNTYuNjYyIEMyMi4xNTYsNTYuMDA3IDIxLjk0Niw1NS4zMjQgMjEuOTQ2LDU0LjkgQzIxLjk0Niw1NC4zNTYgMjIuMTc2LDU0LjI2NiAyMi4zMjksNTQuMjY2IEMyMi43MzksNTQuMjY2IDIyLjg3LDU1LjA2OCAyMy4zMzUsNTUuMDY4IEMyMy43NjksNTUuMDY4IDIzLjkxNCw1NC41NzggMjMuOTE0LDU0LjUyMyBDMjMuOTE0LDU0LjQ4OCAyMy44ODMsNTQuNDc3IDIzLjg0NCw1NC40NzcgQzIzLjAwNiw1NC40NzcgMjMuMTg2LDUzLjU5MiAyMi4yMTgsNTMuNTkyIEMyMS4zMTUsNTMuNTkyIDIwLjg4Miw1NC4yMzcgMjAuODgyLDU0Ljg4NCBDMjAuODgyLDU2LjI0IDIyLjMxNiw1Ny4xODEgMjIuMzE2LDU4LjI1MSBDMjIuMzE2LDU4LjczMSAyMS45ODYsNTguODIgMjEuNzY3LDU4LjgyIEMyMS42NTgsNTguODIgMjEuNTE1LDU4Ljc2OCAyMS4zNTIsNTguNjc4IEMyMS40MDE3MjYxLDU4LjU2NzQzMTkgMjEuNDI2MzE2Myw1OC40NDcyMTMyIDIxLjQyNCw1OC4zMjYgQzIxLjQyNCw1Ny45NTYgMjEuMTcyLDU3LjY1NiAyMC44ODUsNTcuNjU2IEMyMC43MDMsNTcuNjU2IDIwLjU1Miw1Ny43NTEgMjAuNDQxLDU3LjkwNSBDMTkuOTU1LDU3LjM2NiAxOS41NDQsNTYuNjcgMTkuNTQ0LDU2LjA0MyBDMTkuNTQ0LDU1LjcxOSAxOS42OSw1NS42MzcgMTkuNzkxLDU1LjYzNyBDMjAuMDI2LDU1LjYzNyAyMC4wMzIsNTUuOTUzIDIwLjM0OCw1NS45NTMgQzIwLjU4NCw1NS45NTMgMjAuNzI4LDU1LjY2MiAyMC43MjgsNTUuNDkzIEMyMC43MjgsNTUuNDQ3IDIwLjY2Miw1NS40NjIgMjAuNTk5LDU1LjQ2MiBDMjAuMDg0LDU1LjQ2MiAyMC4xOCw1NS4wNjkgMTkuNTc2LDU1LjA2OSBDMTkuMzcyLDU1LjA2OSAxOC42ODUsNTUuMTc5IDE4LjY4NSw1Ni4xNTEgQzE4LjY4NSw1Ny42MjcgMjAuNTA2LDU4LjA3MiAyMC4wODMsNTkuMjA1IEMyMC44NTYsNTkuMDkyIDIwLjg0Niw1OS44NDQgMjAuODYxLDYxLjE5MiBDMjAuODYxLDYxLjE5MiAyMi4zNjcsNjAuNjgzIDI1LjAzNSw2MC42ODMgTDI1LjAzNCw2MC42ODMgWiBNMjAuMTQxLDYyLjY3NCBMMTguNjYsNjMuMDU1IEMxOC42NzIsNjIuNzM0IDE4Ljc5NCw2Mi40MzcgMTkuMDgsNjIuMzcxIEMxOS4xMTcsNjIuMzYyIDE5LjEyNiw2Mi4zMTMgMTkuMDkyLDYyLjI5IEMxOC40NDUsNjEuODYgMTguMDk4LDYyLjY4NCAxOC4wODgsNjIuNzExIEMxNy44NTgsNjIuNjk0IDE3LjUwOSw2Mi43NjUgMTcuNTA5LDYzLjA3NiBDMTcuNTA5LDY0LjAwMyAxNS4zODgsNjQuNTE4IDE0Ljg5Nyw2NC41OTQgQzE1Ljc2Myw2NC45MjggMTYuMTI0LDY2LjAyIDE2LjEyNCw2Ni4wMiBDMTYuNzUzLDY1LjM5MSAxNy40ODgsNjQuODM3IDE3LjY1LDY0LjgzNyBDMTcuODE3LDY0LjgzNyAxNy44ODEsNjUuMDQ3IDE3Ljg5NCw2NS4xNzEgQzE3Ljg5OCw2NS4yMTIgMTcuOTM2LDY1LjIyNSAxNy45NjIsNjUuMTkgQzE4LjI5MSw2NC42OTkgMTguMTMzLDY0LjU4OCAxOC4xMDcsNjQuNDQzIEMxOC4wODYsNjQuMzI5IDE4LjExOSw2My44OTkgMTguNTk5LDYzLjk1IEwxOC41OTksNzAuMTM4IEwxOC4zNTIsNzAuMTg3IEMxOC4yMzksNzAuMjA3IDE3LjMzNCw2OS40MzcgMTYuMjk4LDY4LjcyMSBDMTUuMjYyLDY4LjAwNSAxNC43MDUsNjguNDMzIDEzLjUyNCw2OS4wNzMgQzExLjk3OSw2OS45MSAxMC40MTUsNjkuMTUxIDEwLjQxNSw2OS4xNTEgQzkuMDgyLDcwLjEwMyA4LjYyOCw3Mi44NTcgOC42MjgsNzIuODU3IEM4LjI3MTU1NjQyLDczLjAwNDI5MjcgNy44OTA2MTk4Myw3My4wODMzMjk0IDcuNTA1LDczLjA5IEM2LjQ4Nyw3My4wOSA2LjMsNzIuNDc1IDYuMyw3Mi4xMDYgQzYuMyw3MC41MzUgOC4yNjcsNjkuODYyIDguMjY3LDY4LjMxNCBDOC4yNjcsNjcuOTM5IDguMDk0LDY2LjQxIDYuMjA0LDY2LjQxIEM1Ljk2Miw2Ni40MSA0LjUxOSw2Ni40MDcgNC41MTMsNjYuNDA3IEMzLjgwNSw2Ni40MDcgMy42MjgsNjUuODk4IDMuNTMsNjUuNzAzIEMzLjQ5Miw2NS42MjcgMy40Miw2NS42NzEgMy40NDcsNjUuNzMxIEMzLjUwMSw2NS44NjEgMy4zNjUsNjYuMDU5IDMuMzY1LDY2LjQ1MSBDMy4zNjUsNjcuMDggMy43MzQsNjcuNDU4IDQuNDIyLDY3LjQ1OCBDNC43MzUsNjcuNDU4IDUuMDMsNjcuMzY5IDUuMTI1LDY3LjMxMiBDNS4xODksNjcuMjc1IDUuMjIzLDY3LjMzNiA1LjE5Myw2Ny4zNyBDNC44OTMsNjcuNzIgNC44NjksNjguMzc1IDQuOTkzLDY4LjUzNSBDNS4wMjIsNjguNTczIDUuMDU5LDY4LjU2MyA1LjA2Niw2OC41MzIgQzUuMjMyLDY3Ljg2OCA1Ljg1NCw2Ny4yNCA2LjY5Miw2Ny4yNCBDNy43MTcsNjcuMjQgNy43MTIsNjguMTEyIDcuNzEyLDY4LjMyMiBDNy43MTIsNjkuNTgyIDUuNDk5LDcwLjUwNyA1LjQ5OSw3Mi4yMzkgQzUuNDk5LDczLjk5NyA3LjM1NSw3NC4zNzMgOC40MzEsNzQuMDk1IEM4LjM2OCw3NS44NTQgNi4zNTQsNzYuNDU0IDYuMjkzLDc1LjE5NSBDNi4yODgsNzUuMTM1IDYuMjQ1LDc1LjEyMSA2LjIxMiw3NS4yIEM1Ljk1Nyw3NS44MjIgNi4wNTgsNzYuMzY5IDYuODQyLDc2LjU0MiBDNi44OTUsNzYuNTU1IDYuODY5LDc2LjU4NCA2LjgyMSw3Ni42MTggQzYuMDQ1LDc3LjExMSA2LjQ2Niw3OS4wNTEgNi41MDQsNzkuNjIxIEM2LjU2Miw4MC40NDMgNS43NDUsODAuMzQ1IDUuNjc4LDgwLjMyMSBDNS42NDQsODAuMzA4IDUuNjEyLDgwLjMyNSA1LjY2Myw4MC4zOSBDNi4yNDMsODEuMTAxIDYuOTU5LDgwLjQ1MSA2Ljk1OSw4MC40NTEgQzcuNSw4MC42MjEgNy4xNjUsODEuMDgyIDYuODA0LDgxLjMyNCBDNi43NDgsODEuMzYyIDYuNzUyLDgxLjQgNi44MTMsODEuNDA0IEM2Ljg5NCw4MS40MDkgNy43MzIsODEuNDggNy44ODEsODAuODEyIEM4LjI1Miw4MS4yNTUgOC43OTcsODEuMDkzIDguOTEyLDgxLjAzNSBDOS4wNzMsODAuOTUzIDkuODU4LDgwLjY2OCA5LjkzNiw4MS41NSBDOS45NDEsODEuNjE0IDkuOTc4LDgxLjU3MyAxMC4wMDQsODEuNTM5IEMxMC41OTcsODAuNzk0IDkuOTYyLDgwLjMzMyA5LjYwOSw4MC4yNzkgQzkuNjM4LDgwLjIzOCAxMC4xMTMsNzkuODQ0IDEwLjU0LDgwLjM5IEMxMC41NjksODAuNDI3IDEwLjU5Niw4MC40MzUgMTAuNjAxLDgwLjM2MSBDMTAuNjUsNzkuNDE1IDkuODgxLDc5LjMyOSA5LjM4Myw3OS43NCBDOS4zNiw3OS42NDMgOS4xOTQsNzkuMTExIDkuOTQ2LDc5LjEzIEM5Ljk2NCw3OS4xMyAxMCw3OS4xMDMgOS45NjIsNzkuMDY2IEM5LjM3OCw3OC40NDkgOC44OTgsNzkuMDEgOC43ODcsNzkuMTQ0IEM4LjUyNyw3OS40NSA3Ljg4LDc5LjMyOCA3LjgyLDc5LjMyIEM4LjAyMyw3Ni4wMzMgMTAuOTE5LDc3LjE5OCAxMS4zMTcsNzUuNiBDMTEuMzM2LDc1LjUxNSAxMS4zNzMsNzUuNTQ3IDExLjM4MSw3NS41NzcgQzExLjQ5LDc1Ljk2NyAxMi40NzMsNzYuMDIxIDEyLjY1NCw3NS4yNTMgQzEyLjY2NCw3NS4yMDUgMTIuNjU4LDc1LjE2OCAxMi42MjMsNzUuMjA0IEMxMS43ODUsNzYuMDQxIDkuODk3LDczLjQyNCAxMi40NzMsNzEuMjk3IEMxNC42NzIsNjkuNDg0IDE2LjY0Myw3MC40OTUgMTcuMzkzLDcxLjA5MSBDMTcuNDMzLDcxLjM1NCAxNy4yNzYsNzEuNDM4IDE3LjE3Niw3MS40NyBDMTcuMTQsNzEuNDgxIDE3LjEyOSw3MS41MTQgMTcuMTg2LDcxLjUzNiBDMTcuMzk2LDcxLjYxNiAxNy42OTEsNzEuNTkgMTcuODI5LDcxLjM5NCBDMTguMzAxLDcxLjcyNCAxNy44OTcsNzIuMTM5IDE3LjcwNiw3Mi4yNTUgQzE3LjY1OCw3Mi4yODUgMTcuNjc5LDcyLjMxNyAxNy43MTksNzIuMzI1IEMxOC4xNTMsNzIuNDAzIDE4LjUxLDcxLjk2IDE4LjUyNyw3MS43MDYgTDE4LjYsNzEuNzQ3IEwxOC42LDczLjYxNSBDMTguNiw3Ni4yNzUgMjEuNjY4LDc2LjY0MyAyNS4wMzUsNzguODk4IEMyOC40MDIsNzYuNjQzIDMxLjQ3LDc2LjI3NCAzMS40Nyw3My42MTUgTDMxLjQ3LDcxLjc0NyBMMzEuNTQ0LDcxLjcwNiBDMzEuNTYxLDcxLjk2MSAzMS45MTgsNzIuNDA0IDMyLjM1Miw3Mi4zMjUgQzMyLjM5Miw3Mi4zMTcgMzIuNDEzLDcyLjI4NSAzMi4zNjUsNzIuMjU1IEMzMi4xNzQsNzIuMTQgMzEuNzcsNzEuNzI0IDMyLjI0Miw3MS4zOTQgQzMyLjM4LDcxLjU5IDMyLjY3Niw3MS42MTYgMzIuODg1LDcxLjUzNiBDMzIuOTQxLDcxLjUxMyAzMi45MzEsNzEuNDggMzIuODk0LDcxLjQ3IEMzMi43OTQsNzEuNDM4IDMyLjYzOCw3MS4zNTMgMzIuNjc4LDcxLjA5MSBDMzMuNDI4LDcwLjQ5NCAzNS4zOTksNjkuNDgzIDM3LjU5OCw3MS4yOTcgQzQwLjE3NCw3My40MjMgMzguMjg2LDc2LjA0MSAzNy40NDgsNzUuMjA0IEMzNy40MTMsNzUuMTY4IDM3LjQwOCw3NS4yMDUgMzcuNDE4LDc1LjI1MyBDMzcuNTk4LDc2LjAyMSAzOC41ODEsNzUuOTY3IDM4LjY5LDc1LjU3NyBDMzguNjk4LDc1LjU0NiAzOC43MzYsNzUuNTE1IDM4Ljc1NSw3NS42IEMzOS4xNTMsNzcuMTk3IDQyLjA0OSw3Ni4wMzIgNDIuMjUyLDc5LjMyIEM0Mi4xOTIsNzkuMzI4IDQxLjU0NSw3OS40NSA0MS4yODUsNzkuMTQ0IEM0MS4xNzQsNzkuMDEgNDAuNjkzLDc4LjQ0OSA0MC4xMSw3OS4wNjYgQzQwLjA3Miw3OS4xMDMgNDAuMTA3LDc5LjEzIDQwLjEyNiw3OS4xMyBDNDAuODc4LDc5LjExMSA0MC43MTIsNzkuNjQzIDQwLjY4OCw3OS43NCBDNDAuMTkxLDc5LjMyOSAzOS40MjEsNzkuNDE1IDM5LjQ3LDgwLjM2MSBDMzkuNDc1LDgwLjQzNSAzOS41MDIsODAuNDI3IDM5LjUzMSw4MC4zOSBDMzkuOTU4LDc5Ljg0NCA0MC40MzMsODAuMjM3IDQwLjQ2Miw4MC4yNzkgQzQwLjEwOSw4MC4zMzMgMzkuNDc0LDgwLjc5NCA0MC4wNjcsODEuNTM5IEM0MC4wOTIsODEuNTcyIDQwLjEyOSw4MS42MTMgNDAuMTM1LDgxLjU1IEM0MC4yMTMsODAuNjY4IDQwLjk5Nyw4MC45NTMgNDEuMTU5LDgxLjAzNSBDNDEuMjc0LDgxLjA5MyA0MS44MTgsODEuMjU1IDQyLjE5LDgwLjgxMiBDNDIuMzM4LDgxLjQ3OSA0My4xNzcsODEuNDA5IDQzLjI1OCw4MS40MDQgQzQzLjMxOCw4MS40IDQzLjMyMyw4MS4zNjMgNDMuMjY3LDgxLjMyNCBDNDIuOTA2LDgxLjA4MyA0Mi41NzEsODAuNjIxIDQzLjExMiw4MC40NTEgQzQzLjExMiw4MC40NTEgNDMuODI4LDgxLjEwMSA0NC40MDgsODAuMzkgQzQ0LjQ1OSw4MC4zMjUgNDQuNDI2LDgwLjMwOCA0NC4zOTMsODAuMzIxIEM0NC4zMjYsODAuMzQ1IDQzLjUwOCw4MC40NDMgNDMuNTY3LDc5LjYyMSBDNDMuNjA1LDc5LjA1MSA0NC4wMjYsNzcuMTExIDQzLjI1LDc2LjYxOCBDNDMuMjAxLDc2LjU4NSA0My4xNzUsNzYuNTU2IDQzLjIyOSw3Ni41NDIgQzQ0LjAxMyw3Ni4zNjggNDQuMTE0LDc1LjgyMiA0My44NTksNzUuMiBDNDMuODI2LDc1LjEyIDQzLjc4Miw3NS4xMzUgNDMuNzc4LDc1LjE5NSBDNDMuNzE3LDc2LjQ1NCA0MS43MDMsNzUuODU0IDQxLjY0LDc0LjA5NSBDNDIuNzE2LDc0LjM3NCA0NC41NzIsNzMuOTk3IDQ0LjU3Miw3Mi4yMzkgQzQ0LjU3Miw3MC41MDcgNDIuMzU5LDY5LjU4MiA0Mi4zNTksNjguMzIyIEM0Mi4zNTksNjguMTExIDQyLjM1NCw2Ny4yNCA0My4zNzksNjcuMjQgQzQ0LjIxNyw2Ny4yNCA0NC44MzksNjcuODY3IDQ1LjAwNSw2OC41MzIgQzQ1LjAxMiw2OC41NjMgNDUuMDQ5LDY4LjU3MyA0NS4wNzgsNjguNTM1IEM0NS4yMDEsNjguMzc2IDQ1LjE3OCw2Ny43MjEgNDQuODc4LDY3LjM3IEM0NC44NDcsNjcuMzM1IDQ0Ljg4MSw2Ny4yNzUgNDQuOTQ2LDY3LjMxMiBDNDUuMDQxLDY3LjM2OSA0NS4zMzYsNjcuNDU4IDQ1LjY0OCw2Ny40NTggQzQ2LjMzNiw2Ny40NTggNDYuNzA1LDY3LjA4IDQ2LjcwNSw2Ni40NTEgQzQ2LjcwNSw2Ni4wNTggNDYuNTcsNjUuODYxIDQ2LjYyNCw2NS43MzEgQzQ2LjY1MSw2NS42NzEgNDYuNTc4LDY1LjYyOCA0Ni41NDEsNjUuNzAzIEM0Ni40NDIsNjUuODk4IDQ2LjI2NSw2Ni40MDcgNDUuNTU4LDY2LjQwNyBMNDMuODY2LDY2LjQxIEM0MS45NzYsNjYuNDEgNDEuODAzLDY3LjkzOCA0MS44MDMsNjguMzE0IEM0MS44MDMsNjkuODYyIDQzLjc2OSw3MC41MzYgNDMuNzY5LDcyLjEwNiBDNDMuNzY5LDcyLjQ3NSA0My41ODMsNzMuMDkgNDIuNTY1LDczLjA5IEM0Mi4xNDgsNzMuMDkgNDEuNzA3LDcyLjk3MSA0MS40NDMsNzIuODU3IEM0MS40NDMsNzIuODU3IDQwLjk4OSw3MC4xMDMgMzkuNjU2LDY5LjE1MSBDMzkuNjU2LDY5LjE1MSAzOC4wOTIsNjkuOTEgMzYuNTQ3LDY5LjA3MyBDMzUuMzY1LDY4LjQzNCAzNC44MDgsNjguMDA1IDMzLjc3Miw2OC43MjEgTDMxLjcxOSw3MC4xODcgTDMxLjQ3Miw3MC4xMzggTDMxLjQ3Miw2My45NSBDMzEuOTUyLDYzLjkgMzEuOTg2LDY0LjMyOSAzMS45NjUsNjQuNDQzIEMzMS45MzksNjQuNTg4IDMxLjc4MSw2NC42OTkgMzIuMTEsNjUuMTkgQzMyLjEzNSw2NS4yMjYgMzIuMTc0LDY1LjIxMyAzMi4xNzgsNjUuMTcxIEMzMi4xOTEsNjUuMDQ2IDMyLjI1NSw2NC44MzcgMzIuNDIyLDY0LjgzNyBDMzIuNTg0LDY0LjgzNyAzMy4zMiw2NS4zOTEgMzMuOTQ5LDY2LjAyIEMzMy45NDksNjYuMDIgMzQuMzEsNjQuOTI4IDM1LjE3Niw2NC41OTQgQzM0LjY4NCw2NC41MTggMzIuNTYzLDY0LjAwNCAzMi41NjMsNjMuMDc2IEMzMi41NjMsNjIuNzY2IDMyLjIxNCw2Mi42OTQgMzEuOTg1LDYyLjcxMSBDMzEuOTc0LDYyLjY4NCAzMS42MjcsNjEuODYxIDMwLjk4MSw2Mi4yOSBDMzAuOTQ3LDYyLjMxMyAzMC45NTYsNjIuMzYyIDMwLjk5Myw2Mi4zNzEgQzMxLjI3OSw2Mi40MzcgMzEuNDAxLDYyLjczNCAzMS40MTMsNjMuMDU1IEMyOS4zNDE2NzM0LDYyLjQ1MTM0NTQgMjcuMTk2NDU5Niw2Mi4xMzg2ODQgMjUuMDM5LDYyLjEyNiBDMjMuNjg5NTYyNyw2Mi4xMzIwNjYxIDIyLjM0MzMxMDMsNjIuMjU3NTU1NyAyMS4wMTYsNjIuNTAxIEwyMS4wMTYsNjQuNjQ2IEwyMC4xNDYsNjQuNjQ2IEwyMC4xNDYsNjIuNjczIEwyMC4xNDYsNjIuNjczIEwyMC4xNDEsNjIuNjc0IFogTTE0LjM4LDg0LjY0OCBDMTQuMTQ5LDg0LjU1NSAxMy45OTcsODQuNDA2IDEzLjk5Nyw4NC4xMDMgTDEzLjk5Nyw4Mi43NzEgQzEzLjYxNiw4Mi42MzcgMTIuNTcyLDgyLjIzOSAxMC40OTEsODIuMjM5IEM4Ljc2MSw4Mi4yMzkgOC4yMTksODIuNjI5IDguMTk0LDgyLjc2NyBMNy43MzQsODUuMzAzIEM3LjczNCw4NS4zMDMgOC40MzQsODQuNjYyIDEwLjk0Myw4NC44MjcgQzEzLjk2NSw4NS4wMTkgMTcuMDk1LDg2LjgwMiAxNy4wOTUsODQuOTAxIEwxNy4wOTUsODMuNzgzIEMxNC43MjYsODMuNzgzIDE0LjM4LDg0LjUzNiAxNC4zOCw4NC42NDggTDE0LjM4LDg0LjY0OCBaIE0zMi45NzIsODMuNzgzIEwzMi45NzIsODQuOTAxIEMzMi45NzIsODYuODAyIDM2LjEwMyw4NS4wMTkgMzkuMTI0LDg0LjgyNyBDNDEuNjMyLDg0LjY2MyA0Mi4zMzMsODUuMzAzIDQyLjMzMyw4NS4zMDMgTDQxLjg3Myw4Mi43NjcgQzQxLjg0OCw4Mi42MjkgNDEuMzA3LDgyLjIzOSAzOS41NzYsODIuMjM5IEMzNy40OTUsODIuMjM5IDM2LjQ1MSw4Mi42MzcgMzYuMDcxLDgyLjc3MSBMMzYuMDcxLDg0LjEwMyBDMzYuMDcxLDg0LjQwNyAzNS45MTgsODQuNTU1IDM1LjY4OCw4NC42NDggQzM1LjY4OCw4NC41MzYgMzUuMzQsODMuNzgzIDMyLjk3Myw4My43ODMgTDMyLjk3Miw4My43ODMgWiBNMjUuMDM0LDgyLjYzMiBDMjkuOSw4Mi42MzIgMzQuODQsODIuODg3IDM1LjQ4Niw4My42MiBDMzUuNTM4LDgzLjY3OCAzNS41NTksODMuNjE5IDM1LjU1OSw4My41NzUgTDM1LjU2LDgwLjk5NiBDMzUuNTYsODAuMjQ0IDMwLjQ2Miw3OS44MDYgMjUuMDM0LDc5LjgwNiBDMTkuNjA2LDc5LjgwNiAxNC41MDgsODAuMjQ0IDE0LjUwOCw4MC45OTYgTDE0LjUwOSw4My41NzUgQzE0LjUwOSw4My42MTkgMTQuNTMsODMuNjc5IDE0LjU4Miw4My42MiBDMTUuMjI4LDgyLjg4NyAyMC4xNjksODIuNjMyIDI1LjAzNCw4Mi42MzIgWiBNMjAuNjk0LDcwLjQ3MyBMMjAuMjAyLDcwLjM0NiBMMjAuMTYxLDcwLjQwNyBMMTkuNTksNzAuMTU2IEwyMC4xMDEsNzAuMTUyIEMyMC4xNDksNzAuMTUyIDIwLjE1Miw3MC4xMjEgMjAuMTI3LDcwLjA5OCBMMTkuNjkxLDY5LjY3MSBMMjAuMjU1LDY5Ljg4MSBDMjAuMjkzLDY5Ljg5NCAyMC4zMDcsNjkuODY2IDIwLjI4OSw2OS44NDEgTDE5LjkxMyw2OS4yOSBMMjAuNDY2LDY5LjY2IEMyMC40OTYsNjkuNjgxIDIwLjUyLDY5LjY2MSAyMC41MDcsNjkuNjMxIEwyMC4yNDUsNjguOTg0IEwyMC43NSw2OS40NiBDMjAuNzczLDY5LjQ4MSAyMC43OTksNjkuNDY5IDIwLjc5Myw2OS40MzcgTDIwLjY1NCw2OC43OSBMMjEuMDI0LDY5LjMzMSBDMjEuMDQ2LDY5LjM2MyAyMS4wNzQsNjkuMzU0IDIxLjA3NCw2OS4zMTYgTDIxLjA4Niw2OC43MjQgTDIxLjMyMSw2OS4yNjUgQzIxLjMzNyw2OS4zMDIgMjEuMzY1LDY5LjMwNSAyMS4zODUsNjkuMjU3IEwyMS41NzUsNjguNzk5IEwyMS41OTcsNjkuNDE0IEwyMS41MTUsNjkuNDMgTDIxLjQxMiw3MC4xNzIgQzIyLjE0Nyw3MCAyMy4wNjIsNjguNzQzIDIzLjc2NCw2OC43NDMgQzI0LjYsNjguNzQzIDI0LjkwMyw2OS43MjIgMjYuNDQ4LDY5LjIyNiBDMjYuODM3LDY5LjgwMyAyNi45NzQsNzAuMzA1IDI3LjEyNCw3MC45OTggQzI3LjcyLDcxLjU1OSAyOC41NTcsNzEuMzcyIDI4LjU1Nyw3MC43NzggQzI4LjU1Nyw2OS45NzggMjcuNjg1LDY5LjgyNiAyNy42ODUsNjguOTkxIEMyNy42ODUsNjguNjE4IDI3Ljk0Niw2OC4wODIgMjguNjU5LDY4LjA4MiBDMjkuMDM2LDY4LjA4MiAyOS41ODgsNjguMjQ0IDI5LjkwNiw2OC4yNDQgQzMwLjI4Myw2OC4yNDQgMzAuMzY0LDY3Ljk3MSAzMC40MDYsNjcuODU4IEMzMC40NCw2Ny43NzMgMzAuNTQzLDY3LjgwMSAzMC41MzMsNjcuODY3IEMzMC41Miw2Ny45NDUgMzAuNTc3LDY4LjA1NCAzMC41NzcsNjguMjMzIEMzMC41NzcsNjguNzYxIDI5LjgwOSw2OC44MyAyOS43MTQsNjguNzY1IEMyOS42NzQsNjguNzM3IDI5LjY0Niw2OC43OCAyOS42NzQsNjguODAyIEMyOS43ODksNjguODk1IDI5Ljc2NCw2OS4yMDkgMjkuNjI2LDY5LjQxIEMyOS42MDksNjkuNDMzIDI5LjU4OCw2OS40MjggMjkuNTg0LDY5LjQwNyBDMjkuNTAyLDY5LjAyOSAyOS4wNTgsNjguNTYyIDI4LjY2Nyw2OC41NjIgQzI4LjUyMyw2OC41NjIgMjguMjM3LDY4LjY0NiAyOC4yMzcsNjguOTc3IEMyOC4yMzcsNjkuNDUyIDI5LjE4OCw2OS44ODQgMjkuMTg4LDcwLjg2OSBDMjkuMTg4LDcxLjg5MiAyOC4xNzUsNzIuMTM3IDI3LjE1Niw3MS44ODIgQzI3LjE5MSw3Mi44NSAyOC42MzYsNzMuNDI2IDI4LjY3MSw3Mi43MzQgQzI4LjY3Myw3Mi42ODYgMjguNjk5LDcyLjcxNiAyOC43MTQsNzIuNzM1IEMyOC45MjksNzMuMDMxIDI4LjgzMSw3My4yNTYgMjguNTU3LDczLjM3IEMyOC44MDksNzMuNDg0IDI4Ljg1NCw3My45OTIgMjguOTI5LDc0LjI5NiBDMjkuMDQsNzQuNzUxIDI5LjUyMyw3NC40OTkgMjkuNTQ2LDc0LjQ4NiBDMjkuNTY5LDc0LjQ3MyAyOS41ODEsNzQuNDg1IDI5LjU3LDc0LjUxNCBDMjkuMzg3LDc0Ljk4OCAyOC45MDEsNzQuODA4IDI4LjkwMSw3NC44MDggQzI4LjY0Niw3NC45ODcgMjguOTU0LDc1LjE0OCAyOS4xODUsNzUuMjE3IEMyOS4yMiw3NS4yMjYgMjkuMjE3LDc1LjI1MiAyOS4xOSw3NS4yNjIgQzI5LjAyLDc1LjMyNCAyOC42NTUsNzUuNDM4IDI4LjQ3OSw3NS4xMzMgQzI4LjM0OCw3NS40NDkgMjguMDc5LDc1LjQzOSAyNy45NzUsNzUuNDM5IEMyNy43ODksNzUuNDM5IDI3LjY2NSw3NS42MyAyNy43MDgsNzUuODg1IEMyNy43MTIsNzUuOTA4IDI3LjY5Niw3NS45MTQgMjcuNjc5LDc1LjkwMSBDMjcuNTI4MjM2OSw3NS44NDQ4MDg1IDI3LjQyNzQ1NTMsNzUuNzAxODAwOCAyNy40MjUyNDIzLDc1LjU0MDkyMTcgQzI3LjQyMzAyOTQsNzUuMzgwMDQyNiAyNy41MTk4Mzk1LDc1LjIzNDMxNyAyNy42NjksNzUuMTc0IEMyNy42MzgsNzUuMTU1IDI3LjMzMiw3NS4wMjggMjcuMjAyLDc1LjM4NiBDMjcuMTkyLDc1LjQxNCAyNy4xNzUsNzUuNDE1IDI3LjE2NSw3NS4zODkgQzI2Ljk3MSw3NC44NzMgMjcuMzk0LDc0LjcyMyAyNy42OTksNzQuODUzIEMyNy42NzgsNzQuNzggMjcuNTE4LDc0LjUyOCAyNy4xMTcsNzQuNjA5IEMyNy4wOTksNzQuNjE0IDI3LjA4Nyw3NC42MDYgMjcuMDk5LDc0LjU4MSBDMjcuMTY1LDc0LjQyMyAyNy4zNjQsNzQuMTkgMjcuNzI4LDc0LjQyNiBDMjcuOTE0LDc0LjU0MSAyOC4yMzUsNzQuMzczIDI4LjI2NCw3NC4zNTggQzI3LjYxNyw3My4wNyAyNi4zNTksNzQuMTQ4IDI2LjA3OCw3My4xMzkgQzI2LjA3Myw3My4xMTggMjYuMDQ4LDczLjA5OSAyNi4wMyw3My4xNTUgQzI1Ljk2Myw3My4zNyAyNS40MTQsNzMuMzg0IDI1LjMxNCw3Mi45NjEgQzI1LjMwNiw3Mi45MjggMjUuMzE5LDcyLjkyNCAyNS4zNCw3Mi45NDUgQzI1LjQyLDczLjAyNiAyNS41OTMsNzMuMDk5IDI1LjgxOSw3Mi43NzEgQzI1Ljk2OSw3Mi41NTUgMjUuOTksNzIuMjY2IDI1Ljk5LDcyLjA2OSBDMjUuOTksNzEuMjg1IDI1LjIwNyw2OS45NyAyMy45ODcsNjkuOTcgQzIzLjIxNiw2OS45NyAyMi4zODcsNzAuMjcxIDIxLjc3MSw3MC43MiBMMjIuMzQ0LDcwLjg1MSBDMjIuMjksNzEuNDMxIDIxLjczMiw3MS43NjggMjEuMjIsNzEuNjI2IEwyMS4zMDksNzAuOTYzIEMyMS4yMiw3MC45OTEgMjAuOTk1LDcxLjA3IDIwLjg3NSw3MS4yNDIgQzIwLjg2Niw3MS4yNTUgMjAuODQ5LDcxLjI1MSAyMC44NDUsNzEuMjM3IEMyMC43NCw3MC44NjIgMjEuMDUxLDcwLjY3MiAyMS4xNDcsNzAuNjIzIEMyMC44NTksNzAuNTk4IDIwLjcwMyw3MC44MDIgMjAuNjU5LDcwLjg3MiBDMjAuNjUyLDcwLjg4MyAyMC42NCw3MC44OCAyMC42MzYsNzAuODcxIEMyMC42MTMsNzAuODAyIDIwLjU2NCw3MC42MDggMjAuNjk0LDcwLjQ3IEwyMC42OTQsNzAuNDczIFogTTIyLjM5Niw3NC45NTEgQzIyLjE3Niw3NS4yMTkgMjEuOTg0LDc0Ljk2MyAyMS42OTYsNzQuOTYzIEMyMS40MjUsNzQuOTYzIDIxLjM4LDc1LjE5IDIxLjM3Myw3NS4yOCBDMjEuMzcyLDc1LjI5NiAyMS4zNjIsNzUuMzAzIDIxLjM1MSw3NS4yODUgQzIxLjA0NCw3NC44NjYgMjEuMzg3LDc0LjYzOCAyMS41ODcsNzQuNjA3IEMyMS41NTgsNzQuNTc4IDIxLjI1Niw3NC4zNzUgMjEuMDY0LDc0LjY1MyBDMjEuMDQ5LDc0LjY3MyAyMS4wMzEsNzQuNjY0IDIxLjAzMSw3NC42NCBDMjEuMDIyLDc0LjEyIDIxLjQzNCw3NC4wODMgMjEuNzA2LDc0LjMxMSBDMjEuNzU1LDc0LjExNSAyMS42MjgsNzMuOTgyIDIxLjQwOCw3My45NzEgQzIxLjM4Miw3My45NzEgMjEuMzc3LDczLjk1NCAyMS4zODgsNzMuOTQyIEMyMS40OCw3My44NDMgMjEuNTcsNzMuNzk3IDIxLjY2LDczLjc5NyBDMjEuOTUsNzMuNzk3IDIxLjk4Niw3NC4xNjMgMjIuMTc1LDc0LjE2MyBDMjIuNDU5LDc0LjE2MyAyMi44Niw3My43ODEgMjIuODk5LDczLjc0IEMyMi44OTEsNzMuNzE5IDIyLjYyLDcyLjk4NCAyMi42NzMsNzIuNTQzIEMyMi42NzYsNzIuNTI0IDIyLjY2Niw3Mi41MjIgMjIuNjUzLDcyLjUzIEMyMi41MTYsNzIuNjA3IDIyLjE2OCw3Mi42MDQgMjIuMTY4LDcyLjE3NiBDMjIuMTY4LDcyLjE0MSAyMi4xNzgsNzIuMTM5IDIyLjE5Nyw3Mi4xNjcgQzIyLjM1LDcyLjM3OCAyMi41OTcsNzIuMTQgMjIuNzIsNzIuMDAxIEMyMi44NTQsNzEuODUyIDIzLjU2OCw3MC44MTIgMjQuNTIxLDcwLjgxMiBDMjQuNzMxLDcwLjgxMyAyNS4wMDEsNzAuOTI3IDI1LjIxOSw3MS4wNzkgQzI0Ljg5LDcxLjQ4IDI1LjIxNiw3MS44NjMgMjUuNDYsNzEuODE0IEMyNS40ODksNzEuODA5IDI1LjQ5NCw3MS44MzUgMjUuNDY3LDcxLjg0NSBDMjQuMjA3LDcyLjMxOSAyMy42NDQsNzIuNTA4IDIzLjY0NCw3My41NjMgQzIzLjY0NCw3NC4xMTQgMjMuODMyLDc0LjQxOSAyNC4xNTUsNzQuMzQ3IEMyNC4xOCw3NC4zNCAyNC4xODYsNzQuMzUxIDI0LjE2OSw3NC4zNyBDMjQuMDUyLDc0LjUwOSAyMy45MTcsNzQuNTU2IDIzLjgwNSw3NC41NTYgQzIzLjU0NCw3NC41NTYgMjMuNDk3LDc0LjQxMyAyMy4zNTYsNzQuNDEzIEMyMy4yMTUsNzQuNDEzIDIyLjc0Miw3NC42ODkgMjIuNzQyLDc0LjkyIEMyMi43NDIsNzUuMDA3IDIyLjc5Nyw3NS4xMjYgMjIuOTYzLDc1LjIzNCBDMjIuODUzLDc1LjI3MSAyMi40ODEsNzUuMzAzIDIyLjM5NSw3NC45NTEgTDIyLjM5Niw3NC45NTEgWiBNMjUuNzEzLDY0LjY5NyBDMjUuMzE5LDY0LjY3MyAyNS4wNjcsNjQuNzYxIDI1LjA3OSw2NC44ODcgQzI1LjI3MSw2NS4wODkgMjUuNTI1LDY1LjA2NiAyNS43MTMsNjQuNjk3IFogTTI0LjI2LDY1LjY3MSBDMjQuNDYyMTY3MSw2NS42MjM0ODQgMjQuNjcxNDU1MSw2NS42MTQzMjU2IDI0Ljg3Nyw2NS42NDQgQzI0LjkyMSw2NS42NTkgMjQuOTIsNjUuNzI1IDI0Ljg3LDY1LjczMiBDMjQuMjksNjUuODA2IDI0LjI2Myw2Ni4wNzMgMjMuODkzLDY2LjA3MyBDMjMuNjQ5LDY2LjA3MyAyMy4zODUsNjUuODI4IDIzLjQ4NCw2NS40NjUgQzIzLjQ5Nyw2NS40MTkgMjMuNTMzLDY1LjQxNyAyMy41NDQsNjUuNDYxIEMyMy41ODEsNjUuNjE3IDIzLjgwNyw2NS43ODcgMjQuMjYsNjUuNjcxIEwyNC4yNiw2NS42NzEgWiBNMTYuNzMsNzguNTI5IEMxNy4xMTUsNzkuMDIxIDE3LjQ4Myw3OC41NTggMTcuOTg3LDc4LjU1OCBDMTguMjMyLDc4LjU1OCAxOC41MzMsNzguNjQyIDE4LjU4OSw3OS4xMzUgQzE4LjU5Miw3OS4xNiAxOC42MTMsNzkuMTY4IDE4LjYzNyw3OS4xMzUgQzE5LjE4Niw3OC4zODQgMTguNTYxLDc3Ljk2MSAxOC4yMDEsNzcuOTA3IEMxOC4yNzksNzcuODM3IDE4LjgxNiw3Ny41MDYgMTkuMTU2LDc3Ljk5NCBDMTkuMTc1LDc4LjAyMSAxOS4yMTIsNzguMDEzIDE5LjIxMiw3Ny45NzYgQzE5LjIxMiw3Ny4wMjYgMTguNSw3Ni45NiAxNy45ODUsNzcuMzcxIEMxNy44ODgsNzcuMDAxIDE4LjE2Nyw3Ni43NTcgMTguNTI2LDc2Ljc1NyBDMTguNTY5LDc2Ljc1NyAxOC41ODQsNzYuNzI4IDE4LjU2Miw3Ni43MDMgQzE3LjkyNiw3Ni4wMjEgMTcuNDUsNzYuODc4IDE3LjM0Miw3Ny4wMTUgQzE3LjA4OCw3Ny4zNTMgMTYuMTc5LDc2LjcxOCAxNS44MTYsNzYuMzQgQzE1LjgxNiw3Ni4zNCAxNi42MTksNzUuNDk5IDE2LjYxOSw3NC43MyBDMTYuNjE5LDc0LjY4NSAxNi42MTQsNzQuNjIgMTYuNjE0LDc0LjU5MyBDMTYuNjE0LDc0LjU2NiAxNi42MjUsNzQuNTYgMTYuNjQ2LDc0LjU3MyBDMTYuNzM2Njg4NCw3NC42MjI5ODIzIDE2LjgzODQ1MTMsNzQuNjQ5NDU0NSAxNi45NDIsNzQuNjUgQzE3LjEyNiw3NC42NSAxNy40NzMsNzQuNTkyIDE3LjQ3Myw3My45MzkgQzE3LjQ3Myw3My44OSAxNy40NSw3My44OTMgMTcuNDI5LDczLjkyMiBDMTcuMzM0LDc0LjA2IDE3LjIyOCw3NC4wOSAxNy4xNDcsNzQuMDkgQzE2LjQ5OSw3NC4wOSAxNS4xNDUsNzEuOTI4IDEzLjU1LDcxLjkyOCBDMTMuMTMzLDcxLjkyOCAxMi4zNDQsNzIuMjI5IDExLjk0Niw3Mi42NiBDMTIuMTM5LDcyLjc3NCAxMi4xNzUsNzMuMDA4IDEyLjE1Niw3My4xNzkgQzEyLjEyNyw3My40NTQgMTEuOTA5LDczLjU4OSAxMS42ODYsNzMuNjUxIEMxMS42NDYsNzMuNjYgMTEuNjQzLDczLjY4OSAxMS42ODYsNzMuNzA3IEMxMi40OTIsNzQuMDE1IDEzLjk1OCw3NC41NyAxNC4yOTEsNzQuODU3IEMxNC42ODUsNzUuMTk3IDE0LjYwOCw3NS44NDEgMTQuMzgsNzYuNzI5IEMxNC4xNTksNzcuNTgyIDEzLjY4OCw3Ny40NjUgMTMuNTE0LDc3LjQzNyBDMTMuNDg5LDc3LjQzMyAxMy40OCw3Ny40NDkgMTMuNDk2LDc3LjQ2OSBDMTMuNzA0LDc3LjczMiAxMy45NTcsNzcuODE5IDE0LjE3NCw3Ny44MTkgQzE0LjU5Miw3Ny44MTkgMTQuNzQ5LDc3LjU1OSAxNC45NjYsNzcuNTU5IEMxNS4zMzEsNzcuNTU5IDE2LjA5NCw3OC4wODIgMTYuMDk0LDc4LjQ4IEMxNi4wOTQsNzguNzAzIDE1LjkyLDc4LjkwNiAxNS42OTUsNzkuMDUyIEMxNS42OCw3OS4wNjMgMTUuNjgzLDc5LjA4IDE1LjcwNSw3OS4wODQgQzE1Ljg3OSw3OS4xMTMgMTYuNTk0LDc5LjE3NCAxNi43MzIsNzguNTMxIEwxNi43Myw3OC41MjkgWiBNMzQuMzY0LDc5LjA4MiBDMzQuMzg2LDc5LjA3OCAzNC4zODksNzkuMDYxIDM0LjM3NSw3OS4wNSBDMzQuMTUsNzguOTA0IDMzLjk3NSw3OC43MDEgMzMuOTc1LDc4LjQ3OCBDMzMuOTc1LDc4LjA4IDM0LjczOCw3Ny41NTcgMzUuMTAzLDc3LjU1NyBDMzUuMzIsNzcuNTU3IDM1LjQ3Niw3Ny44MTcgMzUuODk1LDc3LjgxNyBDMzYuMTEyLDc3LjgxNyAzNi4zNjUsNzcuNzMgMzYuNTc0LDc3LjQ2NyBDMzYuNTg5LDc3LjQ0NyAzNi41ODEsNzcuNDMxIDM2LjU1Niw3Ny40MzUgQzM2LjM4Miw3Ny40NjMgMzUuOTEsNzcuNTggMzUuNjksNzYuNzI3IEMzNS40NjIsNzUuODQgMzUuMzg1LDc1LjE5NSAzNS43NzksNzQuODU1IEMzNi4xMTIsNzQuNTY4IDM3LjU3OCw3NC4wMTMgMzguMzg0LDczLjcwNSBDMzguNDI3LDczLjY4OCAzOC40MjQsNzMuNjU5IDM4LjM4NCw3My42NDkgQzM4LjE2MSw3My41ODcgMzcuOTQ0LDczLjQ1MSAzNy45MTQsNzMuMTc3IEMzNy44OTQsNzMuMDA2IDM3LjkzLDcyLjc3MiAzOC4xMjQsNzIuNjU4IEMzNy43MjUsNzIuMjI3IDM2LjkzNSw3MS45MjYgMzYuNTE5LDcxLjkyNiBDMzQuOTIzLDcxLjkyNiAzMy41Nyw3NC4wODggMzIuOTIyLDc0LjA4OCBDMzIuODQxLDc0LjA4OCAzMi43MzYsNzQuMDU4IDMyLjY0MSw3My45MiBDMzIuNjIsNzMuODkxIDMyLjU5Nyw3My44ODggMzIuNTk3LDczLjkzNyBDMzIuNTk3LDc0LjU5IDMyLjk0Myw3NC42NDggMzMuMTI3LDc0LjY0OCBDMzMuMjMxLDc0LjY0OSAzMy4zNDMsNzQuNjE2IDMzLjQyMyw3NC41NzEgQzMzLjQ0NCw3NC41NTggMzMuNDU1LDc0LjU2NyAzMy40NTUsNzQuNTkxIEMzMy40NTUsNzQuNjE1IDMzLjQ1LDc0LjY4MyAzMy40NSw3NC43MjggQzMzLjQ1LDc1LjQ5NyAzNC4yNTQsNzYuMzM4IDM0LjI1NCw3Ni4zMzggQzMzLjg5MSw3Ni43MTYgMzIuOTgyLDc3LjM1MSAzMi43MjgsNzcuMDEzIEMzMi42Miw3Ni44NzYgMzIuMTQ0LDc2LjAxOSAzMS41MDgsNzYuNzAxIEMzMS40ODUsNzYuNzI2IDMxLjUwMSw3Ni43NTUgMzEuNTQ0LDc2Ljc1NSBDMzEuOTAyLDc2Ljc1NSAzMi4xODEsNzYuOTk5IDMyLjA4NSw3Ny4zNjkgQzMxLjU3LDc2Ljk1OCAzMC44NTgsNzcuMDI0IDMwLjg1OCw3Ny45NzQgQzMwLjg1OCw3OC4wMTEgMzAuODk1LDc4LjAxOSAzMC45MTQsNzcuOTkyIEMzMS4yNTQsNzcuNTA0IDMxLjc5MSw3Ny44MzQgMzEuODY4LDc3LjkwNSBDMzEuNTA4LDc3Ljk1OSAzMC44ODMsNzguMzgyIDMxLjQzMiw3OS4xMzMgQzMxLjQ1NSw3OS4xNjYgMzEuNDc2LDc5LjE1OCAzMS40OCw3OS4xMzMgQzMxLjUzNiw3OC42NCAzMS44MzcsNzguNTU2IDMyLjA4MSw3OC41NTYgQzMyLjU4NSw3OC41NTYgMzIuOTUzLDc5LjAxOSAzMy4zMzgsNzguNTI3IEMzMy40NzcsNzkuMTcgMzQuMTkxLDc5LjEwOSAzNC4zNjUsNzkuMDggTDM0LjM2NCw3OS4wODIgWiBNMjEuNDQ5LDY2LjYwOCBDMjEuMzk4LDY2LjY3OCAyMS4yMyw2Ni42MDUgMjEuMTY2LDY2LjU3NiBDMjEuMTk1LDY2LjY3NyAyMS4zNTYsNjYuOTQ3IDIxLjU5Niw2Ni45NDcgQzIxLjcwNSw2Ni45NDcgMjEuODYzLDY2Ljg4MyAyMS45MDgsNjYuODEgTDIzLjMxMyw2Ny4yNDQgQzIzLjEyODQ3MjIsNjcuNDg4OTY5MSAyMy4wMTg3Myw2Ny43ODIwNzgxIDIyLjk5Nyw2OC4wODggQzIyLjYyMiw2Ny44NTcgMjEuMTIzLDY3LjA1MyAyMS4xMjMsNjcuMDUzIEwyMC42NTgsNjcuNjg4IEMyMC41ODEsNjcuNzkzIDIwLjIxLDY3LjUyMiAyMC4yODcsNjcuNDE2IEwyMC42NDIsNjYuOTMxIEMyMC41MjUsNjYuODA2IDIwLjI0MSw2Ni44ODUgMjAuMTY1LDY2LjkwNyBDMjAuMTU0LDY2LjkxIDIwLjE0NCw2Ni45MDQgMjAuMTU0LDY2Ljg4NiBDMjAuMTk5LDY2Ljc5MSAyMC4zNzYsNjYuNDk2IDIwLjgxNyw2Ni42MjkgQzIwLjgxOCw2Ni4zNiAyMC40MTYsNjYuNDQ2IDIwLjMxMiw2Ni40NzUgQzIwLjQxNSw2Ni4zNDQgMjAuNzQyLDY2LjA4IDIxLjEwNyw2Ni4yOTYgTDIxLjI2Niw2Ni4wNzcgTDIwLjk0NCw2NS43MzkgTDIxLjE0Niw2NS40NjIgTDIxLjM3MSw2NS43NjYgTDIzLjEwMiw2My4zOTYgQzIzLjE0NSw2My4zMzggMjMuNDg3LDYzLjIzOCAyMy42MTcsNjMuMjEgQzIzLjYzMSw2My4zNDMgMjMuNjQ1LDYzLjY5OCAyMy42MDQsNjMuNzU2IEwyMS45MDEsNjYuMTQ4IEwyMi4yNiw2Ni4yNjYgTDIyLjA2Miw2Ni41NDQgTDIxLjYzOCw2Ni4zNDYgTDIxLjQ0OCw2Ni42MDcgTDIxLjQ0OSw2Ni42MDggWiBNMjcuNTIsNjYuMjIxIEMyNy41Miw2Ni40MzggMjcuNjU0LDY2Ljc1NyAyNy45NDQsNjYuNjY1IEMyNy45NTksNjYuNjU4IDI3Ljk3Miw2Ni42NjIgMjcuOTY5LDY2LjY3NyBDMjcuOTI1LDY2LjkyIDI3LjQ5Miw2Ny4xNDEgMjcuMjk0LDY2Ljg1NSBDMjcuMTE5LDY3LjIyNSAyNy4xNTksNjcuNTcxIDI3LjUzOCw2Ny43NzYgQzI3LjU1OSw2Ny43ODggMjcuNTYyLDY3LjgxNCAyNy41MzksNjcuODMyIEMyNy4zMTQsNjcuOTk2IDI2Ljk2Miw2Ny45NTkgMjYuNzk4LDY3LjY0NCBDMjYuNDg3LDY3Ljg3NyAyNi41MzMsNjguMzc2IDI2LjcyMSw2OC41NDYgQzI2LjcyMSw2OC41NDkgMjUuODYxLDY5LjA1NyAyNS4wODIsNjguNjkzIEMyNC41NjUsNjguNDQ1IDI0LjE0NCw2OC4xMjggMjMuNDc2LDY4LjE4IEMyMy41NjksNjcuMjQ1IDI0LjAzNiw2Ni45OTggMjQuODk2LDY2Ljg2NCBDMjUuNTgzLDY2Ljc1MyAyNS42NzEsNjYuNDM3IDI1LjU3Miw2Ni4xOTggQzI1LjMzOSw2Ni4xMzYgMjQuOTExLDY2LjM1MSAyNC45MTEsNjYuMzUxIEMyNC44MzQsNjYuMjIyIDI0Ljk0Niw2Ni4wODggMjQuOTQ2LDY2LjA4OCBDMjQuOTAzLDY2LjAzNiAyNC44ODIsNjUuODY0IDI0Ljg4Miw2NS44NjQgQzI1LjM2Miw2NS44NjQgMjUuNDQ3LDY1Ljc1OCAyNS40NDcsNjUuNzU4IEMyNS41MzMsNjUuMjU4IDI1LjAyMyw2NS4yMDUgMjQuNjM1LDY1LjQ3NCBDMjQuNDU1LDY1LjMxNSAyNC40ODQsNjUuMTAyIDI0LjQ4NCw2NS4xMDIgQzI0LjQzLDY1LjA3NiAyNC4zOTcsNjQuOTkyIDI0LjM5Nyw2NC45NTYgQzI0LjM5Nyw2NC44MzggMjQuOTY3LDY0LjUxNyAyNS4xMTMsNjQuNDggQzI1LjE1LDY0LjMxNCAyNS4zNTcsNjQuMDg1IDI1Ljk2Miw2NC4xMzkgQzI2LjAzNCw2My45NzggMjUuOTE3LDYzLjM5MyAyNS44NDMsNjMuMjM2IEMyNS44MTksNjMuMTg4IDI1Ljg1NCw2My4xNjIgMjUuOTA3LDYzLjE5NSBDMjYuMTE5LDYzLjMyOSAyNi4zMjksNjMuODIzIDI2LjQ3Miw2My44MzIgQzI2LjQ3Miw2My44MzIgMjcuMTUzLDYzLjQ3NSAyNy4xODYsNjMuNDk1IEMyNy4yMTksNjMuNTE1IDI3LjMxNSw2NC4yNzQgMjcuMzE1LDY0LjI3NCBDMjcuNDA1LDY0LjM4NSAyNy45MzEsNjQuMjc3IDI4LjE2LDY0LjM3NSBDMjguMjE4LDY0LjQgMjguMjE2LDY0LjQ0MyAyOC4xNjQsNjQuNDUxIEMyNy45OTIsNjQuNDggMjcuNDQ0LDY0LjcxNiAyNy4zNTQsNjQuODY3IEMyNy42NjgsNjUuMzM4IDI3LjUyLDY2LjAzMiAyNy41Miw2Ni4yMjMgTDI3LjUyLDY2LjIyMSBaIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlNoYXBlIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iI0ZGRkZGRiIKICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkPSJNNjEuODc2LDY0LjQ2NSBDNjIuNDgsNjQuMjk0IDYyLjc3NSw2My45ODUgNjIuNzc1LDYzLjI4NiBMNjIuNzc1LDU1LjY3NCBMNjEuNzk4LDU1LjM2MyBMNjEuNzk4LDU1LjAyMiBDNjIuNzc1LDU0Ljg1MiA2My42NTgsNTQuNzkgNjQuODgzLDU0Ljc5IEM2Ny4yMjQsNTQuNzkgNjguMzQsNTUuNzA1IDY4LjM0LDU3LjQxIEM2OC4zNCw1OC45NDUgNjcuNDg3LDU5Ljk1MiA2Ni4xMDgsNjAuMzcxIEw2OC4yOTQsNjMuMzAxIEM2OC42NTc2OTgyLDYzLjc4OTYxMzYgNjkuMTYwMjE3Nyw2NC4xNTcyNjgzIDY5LjczNiw2NC4zNTYgTDY5LjczNiw2NC42NSBDNjkuNDQyLDY0Ljg1MiA2OS4wNTQsNjQuOTkxIDY4LjU1OCw2NC45OTEgQzY3Ljg5MSw2NC45OTEgNjcuNDU3LDY0LjcyOCA2Ni44OTksNjMuODkgTDY0Ljg1Myw2MC43ODkgTDY0LjEyNCw2MC43ODkgTDY0LjEyNCw2NC4wNiBMNjUuMTAxLDY0LjMwOCBMNjUuMTAxLDY0LjY0OSBDNjQuNTg5LDY0LjgwNCA2NC4wMTYsNjQuOTEzIDYzLjMzMyw2NC45MTMgQzYyLjc5MSw2NC45MTMgNjIuMzI2LDY0Ljg2NiA2MS44NzYsNjQuNzU4IEw2MS44NzYsNjQuNDY0IEw2MS44NzYsNjQuNDY1IFogTTY0LjcyOSw2MC4wNDYgQzY1LjkzOCw2MC4wNDYgNjYuOTMsNTkuMjU1IDY2LjkzLDU3LjY0MyBDNjYuOTMsNTYuMzcyIDY2LjEyNCw1NS42NDMgNjQuNzkxLDU1LjY0MyBDNjQuNTg5LDU1LjY0MyA2NC4zMjYsNTUuNjc0IDY0LjEyNCw1NS43MDUgTDY0LjEyNCw2MC4wNDYgTDY0LjcyOCw2MC4wNDYgTDY0LjcyOSw2MC4wNDYgWiBNNzAuNDQ3LDY0LjQ5NSBDNzAuODY2LDY0LjM0MSA3MS4yNjksNjQuMDMgNzEuMjY5LDYzLjY1OCBMNzEuMjY5LDU4LjQxOCBMNzAuMjc3LDU4IEw3MC4yNzcsNTcuNjc0IEM3MC45NzUsNTcuNDcyIDcxLjY4OCw1Ny4zOCA3Mi4xMjIsNTcuMzggTDcyLjYxOCw1Ny4zOCBMNzIuNTQsNTguMjE3IEw3Mi41NCw2NC4xMDkgTDczLjQwOCw2NC4zNDEgTDczLjQwOCw2NC42MzUgQzcyLjg4MSw2NC44MjIgNzIuNDAxLDY0Ljg5OSA3MS42MSw2NC44OTkgQzcxLjI1Myw2NC44OTkgNzAuODAzLDY0Ljg2OCA3MC40NDcsNjQuNzU5IEw3MC40NDcsNjQuNDk1IEw3MC40NDcsNjQuNDk1IFogTTcxLjA5OCw1NS4wNjkgQzcxLjA5OCw1NC41ODkgNzEuNDM5LDU0LjE1NCA3MS45ODIsNTQuMTU0IEM3Mi40MTYsNTQuMTU0IDcyLjc4OCw1NC40OCA3Mi43ODgsNTQuOTkxIEM3Mi43ODgsNTUuNTM0IDcyLjQxNiw1NS45NTIgNzEuOTIsNTUuOTUyIEM3MS40NTUsNTUuOTUyIDcxLjA5OSw1NS41OCA3MS4wOTksNTUuMDY4IEw3MS4wOTgsNTUuMDY5IFogTTc1LjUxNiw1OC40MTggTDc0LjUyMyw1OCBMNzQuNTIzLDU3LjY3NCBDNzUuMjM2LDU3LjQ3MiA3NS45MzQsNTcuMzggNzYuMzgzLDU3LjM4IEw3Ni44NzksNTcuMzggTDc2Ljc4Niw1OC4yNDggTDc2Ljc4Niw2NS4yMDkgQzc2Ljc4Niw2Ny43NTEgNzYuMDI2LDY4LjU0MiA3NS4wMzQsNjguNTQyIEM3NC42OTMsNjguNTQyIDc0LjM1MSw2OC40MTggNzQuMTY2LDY4LjE3IEw3NC4xNjYsNjcuOTIyIEM3NC44NDgsNjcuOTIyIDc1LjUxNSw2Ny4xNDcgNzUuNTE1LDY1LjU2NiBMNzUuNTE1LDU4LjQxOSBMNzUuNTE2LDU4LjQxOCBaIE03NS4zNDUsNTUuMDY5IEM3NS4zNDUsNTQuNTg5IDc1LjY4Nyw1NC4xNTQgNzYuMjI5LDU0LjE1NCBDNzYuNjY0LDU0LjE1NCA3Ny4wMzYsNTQuNDggNzcuMDM2LDU0Ljk5MSBDNzcuMDM2LDU1LjUzNCA3Ni42NjQsNTUuOTUyIDc2LjE2OCw1NS45NTIgQzc1LjcwMiw1NS45NTIgNzUuMzQ2LDU1LjU4IDc1LjM0Niw1NS4wNjggTDc1LjM0NSw1NS4wNjkgWiBNNzguODMyLDY0LjQ5NSBDNzkuMjUxLDY0LjM0MSA3OS42Nyw2NC4wMyA3OS42Nyw2My42NTggTDc5LjY3LDU0LjY2NiBMNzguNjQ3LDU0LjI0NyBMNzguNjQ3LDUzLjkwNiBDNzkuMzYsNTMuNzA0IDgwLjA3Myw1My42MjcgODAuNTA3LDUzLjYyNyBMODEuMDMzLDUzLjYyNyBMODAuOTQsNTQuNDk1IEw4MC45NCw2MC42OTYgTDgxLjc5Myw1OS45NTIgQzgyLjYzMSw1OS4yMzkgODMuMzkxLDU4LjE2OSA4My4zOTEsNTcuNDcyIEM4My42NTUzODQsNTcuMzgzMjgxOCA4My45MzMyMDc1LDU3LjM0MTMyMDYgODQuMjEyLDU3LjM0OCBDODQuNDI2NjY3LDU3LjM0NjUxNDkgODQuNjM4ODAwMiw1Ny4zOTQ0MTYgODQuODMyLDU3LjQ4OCBDODQuODAxLDU4LjE1NSA4NC4xMzUsNTkuMjA5IDgyLjcyNCw2MC4yMTcgTDgyLjE4MSw2MC42MDUgTDg0LjU2OSw2My41MDQgQzg1LjAwMyw2NC4wMzEgODUuMzksNjQuMjQ4IDg1LjcxNiw2NC4yNDggTDg1LjcxNiw2NC41NzQgQzg1LjM3NSw2NC44MDYgODUuMDE4LDY0Ljg5OSA4NC41NjksNjQuODk5IEM4NC4xMzUsNjQuODk5IDgzLjc2Miw2NC43MTMgODMuNDY4LDY0LjMxIEw4MC45NDEsNjAuOTc3IEw4MC45NDEsNjQuMTA5IEw4MS43NDgsNjQuMzU3IEw4MS43NDgsNjQuNjUxIEM4MS4xNzQsNjQuODM4IDgwLjcyNSw2NC44OTkgNzkuOTk2LDY0Ljg5OSBDNzkuNjQsNjQuODk5IDc5LjE4OSw2NC44NjggNzguODMzLDY0Ljc1OSBMNzguODMzLDY0LjQ5NSBMNzguODMyLDY0LjQ5NSBaIE04Ni4xNDgsNjMuMDg1IEM4Ni44NDUsNjMuNzM2IDg3LjUxMiw2NC4wNjIgODguMzM0LDY0LjA2MiBDODkuMjAyLDY0LjA2MiA4OS42NTIsNjMuNjQzIDg5LjY1Miw2My4xMDEgQzg5LjY1Miw2Mi41NTkgODkuMzQxLDYyLjIxNyA4OC4zMTgsNjEuNzgzIEw4Ny41NDQsNjEuNDU3IEM4Ni42NDUsNjEuMDY5IDg2LjE0OCw2MC40MTggODYuMTQ4LDU5LjUzNSBDODYuMTQ4LDU4LjMxIDg3LjIzMyw1Ny4yNTYgODguODQ1LDU3LjI1NiBDOTAuMDg1LDU3LjI1NiA5MC41ODEsNTcuNjI4IDkwLjU4MSw1OC4yMTcgQzkwLjU3ODk1MTgsNTguNDM2MTQ2MSA5MC41NTMxMzQxLDU4LjY1NDQyMzIgOTAuNTA0LDU4Ljg2OCBDOTAuMDA4LDU4LjUyNyA4OS4zMjUsNTguMjc5IDg4LjczNiw1OC4yNzkgQzg3Ljc0NCw1OC4yNzkgODcuMjc5LDU4LjcxMyA4Ny4yNzksNTkuMjQgQzg3LjI3OSw1OS43OTggODcuNTU3LDYwLjEwOCA4OC40ODgsNjAuNDY1IEw4OS4zMjUsNjAuNzkxIEM5MC40NDEsNjEuMjI1IDkwLjg2LDYxLjc5OSA5MC44Niw2Mi43MTMgQzkwLjg2LDYzLjkwNyA4OS43MTMsNjQuOTkyIDg3Ljk3Niw2NC45OTIgQzg2LjYxMiw2NC45OTIgODYuMDA3LDY0LjU1OCA4Ni4wMDcsNjMuNzIxIEM4Ni4wMDcsNjMuNTAzIDg2LjA1NCw2My4yNTUgODYuMTQ3LDYzLjA4NSBMODYuMTQ4LDYzLjA4NSBaIE05NS42MDQsNTcuMjU1IEM5Ny41MSw1Ny4yNTUgOTguOTgzLDU4LjY4MSA5OC45ODMsNjAuODM2IEM5OC45ODMsNjMuMzk0IDk3LjIwMSw2NS4wMDYgOTUuMzU2LDY1LjAwNiBDOTMuNDgsNjUuMDA2IDkxLjk2LDYzLjY4OSA5MS45Niw2MS40MjUgQzkxLjk2LDU4Ljg4MyA5My42OTYsNTcuMjU1IDk1LjYwNCw1Ny4yNTUgTDk1LjYwNCw1Ny4yNTUgWiBNOTUuNDQ5LDY0LjEwOCBDOTYuNDg4LDY0LjEwOCA5Ny41NzMsNjMuMSA5Ny41NzMsNjAuOTc2IEM5Ny41NzMsNTkuMTYyIDk2LjU5Niw1OC4xNTUgOTUuNDk2LDU4LjE1NSBDOTQuNDI2LDU4LjE1NSA5My4zNzIsNTkuMjI0IDkzLjM3Miw2MS4yNzEgQzkzLjM3Miw2My4xNDcgOTQuMzQ5LDY0LjEwOSA5NS40NDksNjQuMTA5IEw5NS40NDksNjQuMTA4IFogTTEwMC4wMjEsNTguMjAxIEw5OS4zODUsNTcuNzUxIEw5OS4zODUsNTcuMzc5IEwxMDIuMzQ3LDU3LjM3OSBMMTAyLjM0Nyw1Ny43NTEgTDEwMS40NjMsNTguMTIzIEwxMDMuMTM3LDYzLjAzOCBMMTAzLjE1Myw2My4wMzggTDEwNC44NzQsNTguMTcgTDEwMy45OSw1Ny43NTIgTDEwMy45OSw1Ny4zOCBMMTA2LjM3OCw1Ny4zOCBMMTA2LjM3OCw1Ny43NTIgTDEwNS45NDQsNTguMTA4IEwxMDMuMTg0LDY0Ljk0NSBMMTAyLjY1Nyw2NC45NzYgTDEwMC4wMjEsNTguMjAxIEwxMDAuMDIxLDU4LjIwMSBaIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlNoYXBlIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iIzAwMDAwMCIKICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkPSJNMTEwLjAzNCw1Ny4yNzEgQzExMS43MDksNTcuMjcxIDExMi43MTcsNTguMzg3IDExMi43MTcsNjAuMTcgTDExMi43MDEsNjEuMTYyIEwxMDguMTI3LDYxLjE2MiBDMTA4LjEyNyw2Mi44OTggMTA5LjEzNSw2My45MDYgMTEwLjQyMiw2My45MDYgQzExMS4xODIsNjMuOTA2IDExMi4wMTksNjMuNjQyIDExMi42MDgsNjMuMjQgQzExMi42NTAzMDEsNjMuMzY5NzgzNSAxMTIuNjcxNTcsNjMuNTA1NDk4IDExMi42NzEsNjMuNjQyIEMxMTIuNjcxLDY0LjM0IDExMS42NDgsNjQuOTkxIDExMC4wMDQsNjQuOTkxIEMxMDguMjM3LDY0Ljk5MSAxMDYuNzk1LDYzLjYxMiAxMDYuNzk1LDYxLjQ0MSBDMTA2Ljc5NSw1OC43NDQgMTA4LjQyMyw1Ny4yNzEgMTEwLjAzNSw1Ny4yNzEgTDExMC4wMzQsNTcuMjcxIFogTTExMS40NzYsNjAuMzU2IEMxMTEuNDc2LDU4LjgzNiAxMTAuODI1LDU4LjEwOCAxMDkuOTEsNTguMTA4IEMxMDkuMDU3LDU4LjEwOCAxMDguMTU4LDU5LjEzMSAxMDguMTU4LDYwLjM1NiBMMTExLjQ3Niw2MC4zNTYgWiIKICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPSJCIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iIzAwMDAwMCIKICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoCiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkPSJNMTE0LjA0OCw2NC40OTUgQzExNC40NTEsNjQuMzQxIDExNC44NjksNjQuMDMgMTE0Ljg2OSw2My42NTggTDExNC44NjksNTguNDE4IEwxMTMuODYyLDU4IEwxMTMuODYyLDU3LjY1OSBDMTE0LjU3NSw1Ny40NTcgMTE1LjI3Miw1Ny4zOCAxMTUuNzIyLDU3LjM4IEwxMTYuMDk0LDU3LjM4IEwxMTYuMDk0LDU4LjQ2NSBMMTE2LjEwOSw1OC40NjUgTDExNi41NDQsNTcuOTg1IEMxMTYuOTYzLDU3LjUzNiAxMTcuMzgxLDU3LjMwMyAxMTcuOTA4LDU3LjMwMyBDMTE4LjQ1MSw1Ny4zMDMgMTE4LjcxNSw1Ny41MzYgMTE4LjcxNSw1Ny45NyBDMTE4LjcwNTYwMiw1OC4yOTUzNTkzIDExOC42MjA3MDEsNTguNjE0MDgwMSAxMTguNDY3LDU4LjkwMSBDMTE4LjE3Miw1OC42NjggMTE3LjgzMSw1OC41NDQgMTE3LjUwNSw1OC41NDQgQzExNy4wMjUsNTguNTQ0IDExNi41NzUsNTguNzYxIDExNi4xNDEsNTkuMTggTDExNi4xNDEsNjQuMTExIEwxMTcuMjExLDY0LjM0MyBMMTE3LjIxMSw2NC42MzcgQzExNi42ODQsNjQuODI0IDExNi4xNzIsNjQuOTAxIDExNS4yODgsNjQuOTAxIEMxMTQuODM5LDY0LjkwMSAxMTQuNDA0LDY0Ljg3IDExNC4wNDgsNjQuNzYxIEwxMTQuMDQ4LDY0LjQ5NyBMMTE0LjA0OCw2NC40OTUgWiBNMTE5LjM0OSw2NC40OTUgQzExOS43NjgsNjQuMzQxIDEyMC4xNyw2NC4wMyAxMjAuMTcsNjMuNjU4IEwxMjAuMTcsNTQuNjY2IEwxMTkuMTYzLDU0LjI0NyBMMTE5LjE2Myw1My45MDYgQzExOS44Niw1My43MDQgMTIwLjQ5Niw1My42MjcgMTIxLjAyMyw1My42MjcgTDEyMS41MTksNTMuNjI3IEwxMjEuNDQxLDU0LjQ5NSBMMTIxLjQ0MSw1OC4yMDEgQzEyMiw1Ny42NTkgMTIyLjcxMiw1Ny4zNDggMTIzLjY4OSw1Ny4zNDggQzEyNS4wMDcsNTcuMzQ4IDEyNS44Niw1OC4xNyAxMjUuODYsNTkuNzY3IEwxMjUuODYsNjQuMTA4IEwxMjYuNjY2LDY0LjM0IEwxMjYuNjY2LDY0LjYzNCBDMTI2LjIzMiw2NC44MDUgMTI1LjY4OSw2NC44OTggMTI1LjAyMiw2NC44OTggQzEyNC43MTIsNjQuODk4IDEyNC4zMjUsNjQuODY3IDEyNC4wMyw2NC43NTggTDEyNC4wMyw2NC40OTQgQzEyNC40MDIsNjQuMzI0IDEyNC41ODksNjQuMDI5IDEyNC41ODksNjMuNDcyIEwxMjQuNTg5LDYwLjUxMSBDMTI0LjU4OSw1OC45OTEgMTIzLjk2OSw1OC40MTggMTIzLjAwOCw1OC40MTggQzEyMi40MDMsNTguNDE4IDEyMS43OTgsNTguNjM1IDEyMS40NDIsNTguODk5IEwxMjEuNDQyLDY0LjEwOCBMMTIyLjMxLDY0LjM0IEwxMjIuMzEsNjQuNjM0IEMxMjEuNzg0LDY0LjgyMSAxMjEuMzAyLDY0Ljg5OCAxMjAuNTEyLDY0Ljg5OCBDMTIwLjE1Niw2NC44OTggMTE5LjcwNSw2NC44NjcgMTE5LjM0OSw2NC43NTggTDExOS4zNDksNjQuNDk0IEwxMTkuMzQ5LDY0LjQ5NSBaIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlNoYXBlIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iIzAwMDAwMCIKICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICAgIDxnCiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZD0iQi1saW5rIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTI3LjAzMzAwMCwgNTcuMDAwMDAwKSIKICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGw9IiMwMDAwMDAiCiAgICAgICAgICAgICAgICAgICAgICAgICAgPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGgKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZD0iTTQuMDM0LDAuMjcxIEM1LjcwOSwwLjI3MSA2LjcxNywxLjM4NyA2LjcxNywzLjE3IEw2LjcwMSw0LjE2MiBMMi4xMjcsNC4xNjIgQzIuMTI3LDUuODk4IDMuMTM1LDYuOTA2IDQuNDIyLDYuOTA2IEM1LjE4Miw2LjkwNiA2LjAxOSw2LjY0MiA2LjYwOCw2LjI0IEM2LjY1MDMwMDkzLDYuMzY5NzgzNTIgNi42NzE1Njk2MSw2LjUwNTQ5Nzk2IDYuNjcxLDYuNjQyIEM2LjY3MSw3LjM0IDUuNjQ4LDcuOTkxIDQuMDA0LDcuOTkxIEMyLjIzNyw3Ljk5MSAwLjc5NSw2LjYxMiAwLjc5NSw0LjQ0MSBDMC43OTUsMS43NDQgMi40MjMsMC4yNzEgNC4wMzUsMC4yNzEgTDQuMDM0LDAuMjcxIFogTTUuNDc2LDMuMzU2IEM1LjQ3NiwxLjgzNiA0LjgyNSwxLjEwOCAzLjkxLDEuMTA4IEMzLjA1NywxLjEwOCAyLjE1OCwyLjEzMSAyLjE1OCwzLjM1NiBMNS40NzYsMy4zNTYgWiIKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IkIiCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgICAgICAgICAgICAgICAgZD0iTTEzNS4wODEsNjQuNDk1IEMxMzUuNSw2NC4zNDEgMTM1LjkwMiw2NC4wMyAxMzUuOTAyLDYzLjY1OCBMMTM1LjkwMiw1OC40MTggTDEzNC45MSw1OCBMMTM0LjkxLDU3LjY3NCBDMTM1LjYwOCw1Ny40NzIgMTM2LjMyMSw1Ny4zOCAxMzYuNzU2LDU3LjM4IEwxMzcuMjUyLDU3LjM4IEwxMzcuMTc0LDU4LjIxNyBMMTM3LjE3NCw2NC4xMDkgTDEzOC4wNDIsNjQuMzQxIEwxMzguMDQyLDY0LjYzNSBDMTM3LjUxNiw2NC44MjIgMTM3LjAzNCw2NC44OTkgMTM2LjI0NCw2NC44OTkgQzEzNS44ODgsNjQuODk5IDEzNS40MzcsNjQuODY4IDEzNS4wODEsNjQuNzU5IEwxMzUuMDgxLDY0LjQ5NSBaIE0xMzUuNzMyLDU1LjA2OSBDMTM1LjczMiw1NC41ODkgMTM2LjA3Myw1NC4xNTQgMTM2LjYxNiw1NC4xNTQgQzEzNy4wNSw1NC4xNTQgMTM3LjQyMiw1NC40OCAxMzcuNDIyLDU0Ljk5MSBDMTM3LjQyMiw1NS41MzQgMTM3LjA1LDU1Ljk1MiAxMzYuNTU0LDU1Ljk1MiBDMTM2LjA4OSw1NS45NTIgMTM1LjczMyw1NS41OCAxMzUuNzMzLDU1LjA2OCBMMTM1LjczMiw1NS4wNjkgWiBNMTQyLjk1NSw1Ny4yODcgQzE0My4zMTQ0NDgsNTcuMjg1Nzc3OCAxNDMuNjczMDQ3LDU3LjMyMTk3MjkgMTQ0LjAyNSw1Ny4zOTUgTDE0NC4wMjUsNTQuNjY2IEwxNDIuOTcsNTQuMjQ3IEwxNDIuOTcsNTMuOTA2IEMxNDMuNjgzLDUzLjcwNCAxNDQuNDQzLDUzLjYyNyAxNDQuODkzLDUzLjYyNyBMMTQ1LjM4OSw1My42MjcgTDE0NS4yOTYsNTQuNDk1IEwxNDUuMjk2LDYzLjM3OCBDMTQ1LjQxMzA1OSw2My43MjI2OTgyIDE0NS42MTU3MjUsNjQuMDMyMDI5NyAxNDUuODg1LDY0LjI3NyBDMTQ1LjU3NCw2NC41NTYgMTQ1LjIzNCw2NC43NzMgMTQ0LjgsNjUuMDA2IEMxNDQuNDg4Mzk5LDY0Ljc1MjcyMDMgMTQ0LjIzNDcwMiw2NC40MzU1OTg1IDE0NC4wNTYsNjQuMDc2IEMxNDMuNTYsNjQuNTg4IDE0Mi43MDYsNjQuOTYgMTQyLjA3Miw2NC45NiBDMTQwLjUwNiw2NC45NiAxMzkuMjUsNjMuNjU3IDEzOS4yNSw2MS41OTUgQzEzOS4yNSw1OC44MDQgMTQwLjk0LDU3LjI4NSAxNDIuOTU1LDU3LjI4NSBMMTQyLjk1NSw1Ny4yODcgWiBNMTQ0LjA0MSw2My40NTcgTDE0NC4wNDEsNTguMzI1IEMxNDMuNzc3LDU4LjIzMiAxNDMuMzI3LDU4LjE1NSAxNDIuOTA5LDU4LjE1NSBDMTQxLjU5MSw1OC4xNTUgMTQwLjU5OCw1OS4xOTQgMTQwLjU5OCw2MS4zNDggQzE0MC41OTgsNjMuMDM4IDE0MS40ODIsNjMuOTUzIDE0Mi40NTgsNjMuOTUzIEMxNDMuMDQ4LDYzLjk1MyAxNDMuNjgzLDYzLjc1MSAxNDQuMDQsNjMuNDU3IEwxNDQuMDQxLDYzLjQ1NyBaIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9IlNoYXBlIgogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD0iIzAwMDAwMCIKICAgICAgICAgICAgICAgICAgICAgICAgICAvPgogICAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgICA8L3N2Zz4=" alt="Logo Rijksoverheid, ga naar de startpagina">
                            </a>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="layout-header__footer">
                   <div class="branding--text">Rijksoverheid</div>
                </div>
             </header>
             <main role="main" class="layout-main">
                <div class="container">
                   <div class="layout-content grid">
                      <div class="region region-content">
                        ${text}
                      </div>
                   </div>
                </div>
             </main>
          </div>
       </body>
    </html>`

    const parsed = load(html)
    const parsedHtml = parsed.html()
    const fileExtension = 'html'
    const fileName = `index.${fileExtension}`
    const documentId = job.recordId.split('|||')[1]
    const staticBucketPath = `${documentId}/${fileName}`

    const fileAttributes = {
        'Content-Type': 'text/html',
    }

    await minioClient.putObject(
        MINIO_OUTPUT_BUCKET_NAME,
        `${job.recordId}/${jobId}/${fileName}`,
        parsedHtml,
        fileAttributes
    )

    await minioClient.putObject(
        MINIO_STATIC_ASSETS_BUCKET_NAME,
        staticBucketPath,
        parsedHtml,
        fileAttributes
    )

    return {
        success: true,
        fileResult: {
            bucketName: MINIO_STATIC_ASSETS_BUCKET_NAME,
            bucketPath: staticBucketPath,
            fileExtension: fileExtension,
        },
        confidence: 100,
    }
}
