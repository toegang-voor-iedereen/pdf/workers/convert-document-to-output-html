import { describe, it, expect } from 'vitest'
import { sleep } from './index.mjs'

describe('utils', () => {
    describe('sleep', () => {
        it('should sleep for a set amount of milliseconds', async () => {
            const sleepMs = 200
            const beforeSleep = new Date()
            await sleep(sleepMs)
            expect(new Date().getTime() - beforeSleep.getTime()).toBeGreaterThanOrEqual(
                sleepMs - 10 // A little slack of 50ms because setTimeout is not precise
            )
        })
    })
})
