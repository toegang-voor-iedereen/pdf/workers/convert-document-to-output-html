/* eslint-disable @typescript-eslint/prefer-literal-enum-member */
import { z } from 'zod'

const prefix = 'application/x-nldoc'

export enum ContentLevelEnum {
    Document = `${prefix}.document`,
    Page = `${prefix}.page`,
    Element = `${prefix}.element`,
}

export enum ElementTypeEnum {
    Text = `${ContentLevelEnum.Element}.text`,
    Figure = `${ContentLevelEnum.Element}.figure`,
    Table = `${ContentLevelEnum.Element}.table`,
    List = `${ContentLevelEnum.Element}.list`,
}

export enum ContentClassificationEnum {
    // Document
    Document = ContentLevelEnum.Document,
    // Page
    Page = ContentLevelEnum.Page,
    // Text
    Text = ElementTypeEnum.Text,
    Line = `${ElementTypeEnum.Text}+line`,
    Paragraph = `${ElementTypeEnum.Text}+paragraph`,
    Title = `${ElementTypeEnum.Text}+title`,
    Heading = `${ElementTypeEnum.Text}+heading`,
    Heading1 = `${ElementTypeEnum.Text}+heading.1`,
    Heading2 = `${ElementTypeEnum.Text}+heading.2`,
    Heading3 = `${ElementTypeEnum.Text}+heading.3`,
    Heading4 = `${ElementTypeEnum.Text}+heading.4`,
    Heading5 = `${ElementTypeEnum.Text}+heading.5`,
    Heading6 = `${ElementTypeEnum.Text}+heading.6`,
    Word = `${ElementTypeEnum.Text}+word`,
    Glyph = `${ElementTypeEnum.Text}+glyph`,
    // Figure
    Figure = ElementTypeEnum.Figure,
    Logo = `${ElementTypeEnum.Figure}+logo`,
    Photo = `${ElementTypeEnum.Figure}+photo`,
    // Table
    Table = ElementTypeEnum.Table,
    TableRow = `${ElementTypeEnum.Table}+row`,
    TableColumn = `${ElementTypeEnum.Table}+column`,
    TableCell = `${ElementTypeEnum.Table}+cell`,
    // List
    List = ElementTypeEnum.List,
    ListItem = `${ElementTypeEnum.List}+item`,
}

export const ContentLevel = z.nativeEnum(ContentLevelEnum)
export const ElementType = z.nativeEnum(ElementTypeEnum)
export const ContentClassification = z.nativeEnum(ContentClassificationEnum)

export const elementTypeFromClassification = (classification: ContentClassificationEnum) => {
    const firstPart = classification.split('+')[0]
    const parsedToTypeEnumValue = ElementType.safeParse(firstPart)
    if (!parsedToTypeEnumValue.success) {
        throw new Error(
            `Unknown classification "${classification}" - is not mappable to element type`
        )
    }

    return parsedToTypeEnumValue.data
}

export const BoundingBox = z.object({
    top: z.number(),
    left: z.number(),
    bottom: z.number(),
    right: z.number(),
})

const BaseContentDefinition = z.object({
    classification: ContentClassification,
    bbox: BoundingBox,
    confidence: z.number(),
    attributes: z.record(z.string(), z.union([z.string(), z.number(), z.boolean()])),
    labels: z.object({ name: z.string(), confidence: z.number() }).array().optional(),
})

export type ContentDefinition = z.infer<typeof BaseContentDefinition> & {
    children?: ContentDefinition[]
}

export const ContentDefinition: z.ZodType<ContentDefinition> = BaseContentDefinition.extend({
    children: z.lazy(() => ContentDefinition.array()).optional(),
})

export const ContentWorkerResult = z.object({
    traceId: z.string(),
    recordId: z.string(),
    jobId: z.string(),
    timestamp: z.string(),
    success: z.boolean(),
    confidence: z.number(),
    contentResult: ContentDefinition.array(),
    error: z
        .object({
            code: z.number(),
            message: z.string(),
        })
        .optional()
        .nullable(),
})

export const AttributeValuesReport = z.object({
    values: z.array(ContentWorkerResult),
    expectedValues: z.number(),
    isComplete: z.boolean(),
    bestJobId: z.string().optional(),
})

export type AttributeValuesReport = z.infer<typeof AttributeValuesReport>
