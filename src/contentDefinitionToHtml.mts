import { MINIO_STATIC_ASSETS_BUCKET_NAME } from './constants.mjs'
import { ContentClassificationEnum, type ContentDefinition } from './types/Content.js'

export function headingToHtml(element: ContentDefinition): string {
    let tag: string
    switch (element.classification) {
        case ContentClassificationEnum.Heading1:
            tag = 'h1'
            break
        case ContentClassificationEnum.Heading3:
            tag = 'h3'
            break
        case ContentClassificationEnum.Heading4:
            tag = 'h4'
            break
        case ContentClassificationEnum.Heading5:
            tag = 'h5'
            break
        case ContentClassificationEnum.Heading6:
            tag = 'h6'
            break
        case ContentClassificationEnum.Heading2:
        case ContentClassificationEnum.Heading:
        default:
            tag = 'h2'
    }

    // TODO: Support multiple title levels
    return `<${tag}>${element.children
        ?.filter((child) => child.classification === ContentClassificationEnum.Line)
        .map((line) => elementToHtml(line, element))
        .join(' ')}</${tag}>`
}

export const titleToHtml = headingToHtml

export function paragraphToHtml(element: ContentDefinition): string {
    const text = `${element.children
        ?.filter((child) => child.classification === ContentClassificationEnum.Line)
        .map((line) => elementToHtml(line, element))
        .join('\n')}`
    return `<p>${text}</p>`
}

export const textToHtml = paragraphToHtml

export function listToHtml(element: ContentDefinition): string {
    const listItems =
        element.children?.filter(
            (child) =>
                child.classification === ContentClassificationEnum.ListItem ||
                child.classification === ContentClassificationEnum.List
        ) ?? []
    const formattedListItems = listItems.map((line) => elementToHtml(line, element)).join('\n')
    if (listItems[0].labels?.find((label) => label.name === 'starts_numeric')) {
        return `<ol>${formattedListItems}</ol>`
    } else if (listItems[0].labels?.find((label) => label.name === 'starts_with_roman_numeral')) {
        if (listItems[0].labels.find((label) => label.name === 'starts_with_cap')) {
            return `<ol type="I">${formattedListItems}</ol>`
        } else {
            return `<ol type="i">${formattedListItems}</ol>`
        }
    } else if (
        listItems[0].labels?.find((label) => label.name === 'starts_with_single_letter') &&
        !`${listItems[0].attributes.text}`.trim().startsWith('o')
    ) {
        if (listItems[0].labels.find((label) => label.name === 'starts_with_cap')) {
            return `<ol type="A">${formattedListItems}</ol>`
        } else {
            return `<ol type="a">${formattedListItems}</ol>`
        }
    } else {
        return `<ul>${formattedListItems}</ul>`
    }
}

export function listItemToHtml(element: ContentDefinition): string {
    return `<li>${element.children
        ?.map((child) => elementToHtml(child, element))
        .join('\n')
        .replace(
            /^\s*(?:[([*«•-]\s*(?:[a-zA-Z0-9]+|[ivxlcdmIVXLCDM]+)\s*[)\].:-]|[([]\s*[ivxlcdmIVXLCDM]+\s*[)\]]|[*«•-]\s+|\d+[a-zA-Z]?\.\s*|[a-zA-Z]\.\s*|[oO]\s*)\s*/,
            ''
        )}</li>`
}

export function lineToHtml(element: ContentDefinition, parent?: ContentDefinition): string {
    const topLevel = parent?.classification === ContentClassificationEnum.Page
    const line = `${element.children
        ?.filter((child) => child.classification === ContentClassificationEnum.Word)
        .map((word) => elementToHtml(word, element))
        .join(' ')}`
    if (topLevel) {
        return `<p>${line}</p>`
    } else {
        return line
    }
}

export function wordToHtml(element: ContentDefinition): string {
    let word = `${element.attributes.text}`.trim()
    if (
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.exec(
            word.toLowerCase()
        )
    ) {
        word = `<a href="mailto:${word}">${word}</a>`
    }
    if (element.labels?.find((label) => label.name === 'bold')) {
        word = `<strong>${word}</strong>`
    }
    return word
}

export function tableToHtml(element: ContentDefinition): string {
    const rows = `${element.children
        ?.filter((child) => child.classification === ContentClassificationEnum.TableRow)
        .map((word) => elementToHtml(word, element))
        .join('\n')}`
    return `<table>${rows}</table>`
}

export function tableRowToHtml(element: ContentDefinition): string {
    const cells = `${element.children
        ?.filter((child) => child.classification === ContentClassificationEnum.TableCell)
        .map((cell) => elementToHtml(cell, element))
        .join('\n')}`
    return `<tr>${cells}</tr>`
}

export function tableCellToHtml(element: ContentDefinition): string {
    const cell = `${element.children?.map((word) => elementToHtml(word, element)).join(' ')}`
    return `<td>${cell}</td>`
}

export function figureToHtml(element: ContentDefinition): string {
    const filePath = element.attributes.document_id
    const fileName = `${element.attributes.id}.${element.attributes.file_extension}`
    const altText = element.attributes.alt_text || 'Lorem ipsum dolor sit amet'
    const imageSourceUrl = `/${MINIO_STATIC_ASSETS_BUCKET_NAME}/${filePath}/images/${fileName}`
    return `<img src="${imageSourceUrl}" alt="${altText}" />`
}

export function elementToHtml(element: ContentDefinition, parent?: ContentDefinition): string {
    switch (element.classification) {
        case ContentClassificationEnum.Heading:
        case ContentClassificationEnum.Heading1:
        case ContentClassificationEnum.Heading2:
        case ContentClassificationEnum.Heading3:
        case ContentClassificationEnum.Heading4:
        case ContentClassificationEnum.Heading5:
        case ContentClassificationEnum.Heading6:
            return headingToHtml(element)
        case ContentClassificationEnum.Title:
            return titleToHtml(element)
        case ContentClassificationEnum.Paragraph:
            return textToHtml(element)
        case ContentClassificationEnum.Text:
            return paragraphToHtml(element)
        case ContentClassificationEnum.List:
            return listToHtml(element)
        case ContentClassificationEnum.ListItem:
            return listItemToHtml(element)
        case ContentClassificationEnum.Line:
            return lineToHtml(element, parent)
        case ContentClassificationEnum.Word:
            return wordToHtml(element)
        case ContentClassificationEnum.Table:
            return tableToHtml(element)
        case ContentClassificationEnum.TableRow:
            return tableRowToHtml(element)
        case ContentClassificationEnum.TableCell:
            return tableCellToHtml(element)
        case ContentClassificationEnum.Figure:
            return figureToHtml(element)
        default:
            return `<p style="color: red;">Unknown element classified as "${element.classification}"</p>`
    }
}
