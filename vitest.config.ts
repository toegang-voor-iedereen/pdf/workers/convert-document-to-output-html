import { defineConfig } from 'vitest/config'

export default defineConfig({
    plugins: [],
    test: {
        include: ['**/*.{test,spec}.?(c|m)[jt]s?(x)'],
        globals: true,
        clearMocks: true,
        coverage: {
            provider: 'v8',
            reporter: ['text', 'html', 'clover', 'json', 'text-summary', 'cobertura'],
            reportsDirectory: 'coverage',
        },
        reporters: ['junit', 'default'],
        outputFile: 'junit.xml',
        setupFiles: ['__test__/setup.ts'],
    },
})
