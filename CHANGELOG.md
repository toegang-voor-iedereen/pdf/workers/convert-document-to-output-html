## [1.12.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.12.2...1.12.3) (2024-11-18)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.9 ([9a6c75a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/9a6c75a3d2952d23508609cd9f0aa39580e9dc98))

## [1.12.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.12.1...1.12.2) (2024-11-12)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/configs to v1.3.3 ([4529710](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/4529710a7519f3c97067d258a536ac8953fd23fc))

## [1.12.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.12.0...1.12.1) (2024-11-11)


### Bug Fixes

* use correct queue name ([b9261c5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/b9261c541f383105092a46de0156de20e8546a40))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.11.1...1.12.0) (2024-09-12)


### Features

* added volume mount for /tmp ([50eeee3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/50eeee37ea591993499d227f18e5fc3f648cfc20))

## [1.11.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.11.0...1.11.1) (2024-09-05)


### Bug Fixes

* version was not updated by CI ([35d957e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/35d957ed92e8e3e7ec5fd3e43f33597c79b3a850))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.10.0...1.11.0) (2024-09-04)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/configs to v1.3.1 ([49031e3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/49031e30af4d51dd1656df5500c4fc87f5ac2845))
* **deps:** update dependency @toegang-voor-iedereen/configs to v1.3.2 ([39aae15](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/39aae155a953d5ef21a4b0296959543602252b19))


### Features

* added securityContext to container and initContainers ([80bfbc0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/80bfbc09a164a16d059dd3d359f90da7f507a246))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.9.3...1.10.0) (2024-09-03)


### Features

* use shared configs ([cf24d8d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/cf24d8dec17e61d801d6e9449fb536656e848ce3))

## [1.9.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.9.2...1.9.3) (2024-08-28)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.8 ([edebebf](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/edebebf0e29a8377ca2759f37ec2b7dc2e4af83c))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.10 ([4a863b7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/4a863b7ef1038b586b716dffb203a78124b77e5a))
* **deps:** update dependency cheerio to v1.0.0 ([bf3d303](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/bf3d303e82e01107a17e6fa087741f4dea04a322))

## [1.9.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.9.1...1.9.2) (2024-07-22)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.9 ([e8f9561](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/e8f956183e9d611383e630c9e4ed25a76b98e3b8))

## [1.9.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.9.0...1.9.1) (2024-07-04)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.7 ([0e29db3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/0e29db314c68b34fb2db127e78dba14619a26c4b))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.8.0...1.9.0) (2024-06-27)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.117.0 ([9062722](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/9062722415b14f07253e5874d474c04a836e72d4))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.6 ([9d8fb24](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/9d8fb243ec677e032be7cb2330ebd9cf612d9706))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.8 ([9a8e52c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/9a8e52c79e42595c57fd5485afa1329263f07f3f))


### Features

* detect list item types ([8b58a9d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/8b58a9d0842af02e751076ebaa549333debac0d4))
* support heading levels ([0f45925](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/0f459258b27a388363820904bac3f7ce5d535776))
* support initContainers in helm values ([b45a86e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/b45a86e550231aa07432a3f40464e7f3828d14fc))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.7.0...1.8.0) (2024-06-05)


### Features

* support initContainers in helm values ([ceee49b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/ceee49ba0761cbd8153f85df235b0e1d157fb15a))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/compare/1.6.0...1.7.0) (2024-06-04)


### Features

* correct worker name in chart ([a064918](https://gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-html/commit/a064918514d54e212402c187562fd93072e0d270))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.5.0...1.6.0) (2024-05-29)


### Features

* move html converters ([f93e382](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/f93e382d77cde65332742d77d8f976770365ef60))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.4.0...1.5.0) (2024-05-28)


### Bug Fixes

* unit tests fixed ([8938349](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8938349eeaa1d0b933aae612ded7b99ba1389058))
* use logger for error display instead of console ([d7716a6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/d7716a6a81a918d7fffff354e89d2a4a2da6f491))


### Features

* convert image content to img html tags, and publish html to static assets as well ([7536465](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/7536465cd91dd809f2c639a044ae841552cb0b82))
* read bucket names from environment vars ([e37979b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/e37979ba3c71ef7c1564898261327cb349f8f62f))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.14...1.4.0) (2024-05-24)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.3 ([3e14df4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/3e14df4b76dc76a4bc2da86248d5f4b471980ba8))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.4 ([81a1a0f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/81a1a0f17edd6d4c8483e63160953c93b64bbb98))


### Features

* add table and list content types ([9a0f17a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/9a0f17a8ae9cb09ae35e14421775aac0e85720bf))
* apply crude heading ([c78cf74](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/c78cf74c96a0ea15dd3821fed1ff16788cb52de7))
* auto-merge patch updates with renovate ([df1acdb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/df1acdb60006b7870de4aba7bc78058623db90db))
* better sorting of elements ([ceb2059](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/ceb205987377650160d0e2b2207b5b2e71b4c90e))
* handle table and list output ([5faabc6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/5faabc6f0084921ce2c662cbc5ace269d6323217))
* merge duplicate pages ([1b7de18](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/1b7de1868e0e8d8e3ef8b1088d1fb8e0a4332196))
* word formatting ([8cae7b0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8cae7b0d93366e3a46481026c11d42e427d2ef25))

## [1.3.14](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.13...1.3.14) (2024-05-13)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.114.0 ([d096dd2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/d096dd2ab178c0b650cdbc6b2d369bbf36198635))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4.0.1 ([abd75f4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/abd75f43fc5d1ac87449522404b3bde3bdac19d7))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.5 ([e0864b8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/e0864b8c47ba715598a546f200678d79c5ec27af))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.7 ([dfe548b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/dfe548be67c094c1b4a31d38c8d794f88325c3b5))
* **deps:** update dependency zod to v3.23.7 ([c5a0a53](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/c5a0a53c9b8d7609ed366cf0fef1d728403f387d))
* **deps:** update dependency zod to v3.23.8 ([4ad60a0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/4ad60a058fd2ddf0b25c4b98b407c897464ced3e))

## [1.3.13](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.12...1.3.13) (2024-05-06)


### Bug Fixes

* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to v0.1.4 ([1dd4798](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/1dd4798b66576f3e4411fefb69f6e5bda94bd5e4))

## [1.3.12](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.11...1.3.12) (2024-05-06)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.113.0 ([5c0d15f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/5c0d15f1ff3500f963f2b16db4970f1e5e0fa26a))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v4 ([4b7db42](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/4b7db423a2540ca81fd87c5bd3723fe59dc571a9))
* **deps:** update dependency @toegang-voor-iedereen/typescript-logger to ^0.1.0 ([f7384a8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/f7384a8f9de3b383e618b5c42dae74f658747fa6))
* **deps:** update dependency zod to v3.23.5 ([7ad6cfe](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/7ad6cfeb0cccf915cb4a50e391d19e16b9522be7))
* **deps:** update dependency zod to v3.23.6 ([9c44bec](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/9c44bec75da7fcea44181dab66aaa4f78a8fdbdc))

## [1.3.11](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.10...1.3.11) (2024-04-25)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.112.2 ([28ab273](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/28ab273429ff862c9b25b0328ed2b8755921437f))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v3.9.3 ([6a1db9b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/6a1db9bb11d941b7bfa2f95b4ff2214e51265026))

## [1.3.10](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.9...1.3.10) (2024-04-24)


### Bug Fixes

* removed comments from helm chart yaml ([1c2120a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/1c2120a7a9633c826f8a044261b8664473bfc8ee))

## [1.3.9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.8...1.3.9) (2024-04-24)


### Bug Fixes

* force release ([9823891](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/98238914d2d2f7115638a875efeec31cb02ae51d))

## [1.3.8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.7...1.3.8) (2024-04-24)


### Bug Fixes

* trigger release ([e07fcc5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/e07fcc5c89a770e5c61a2a8f6d5bfba3012bca57))

## [1.3.7](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.6...1.3.7) (2024-04-24)


### Bug Fixes

* removed temp space ([c91ead0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/c91ead007a7e2e7b9b9c3b841f2fd5aa64da17ed))

## [1.3.6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.5...1.3.6) (2024-04-24)


### Bug Fixes

* temporary space ([572a411](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/572a411bdfa6210e0c4146ee7056ce664a83c975))

## [1.3.5](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.4...1.3.5) (2024-04-24)


### Bug Fixes

* super important carriage return removed ([e1926c3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/e1926c372b0b44f9b0d4518487f9f6ca3dc5f617))

## [1.3.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.3...1.3.4) (2024-04-24)


### Bug Fixes

* new important carriage return 🐎 ([f1b0330](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/f1b0330f99870d359425e9ba4af64f446f206dab))

## [1.3.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.2...1.3.3) (2024-04-23)


### Bug Fixes

* **deps:** update dependency zod to v3.23.4 ([fcdaca8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/fcdaca80e9c6d3413e188a0a69c8dad48bf5c6ad))

## [1.3.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.1...1.3.2) (2024-04-23)


### Bug Fixes

* mega important carriage return ([0c80c2e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/0c80c2e3887db5f8d953fcc780d090b68d5a4432))

## [1.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/1.3.0...1.3.1) (2024-04-23)


### Bug Fixes

* update README ([0222a23](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/0222a230f97a707e697cd3582830f76226c3927c))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/v1.2.0...1.3.0) (2024-04-23)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.110.1 ([8e7cbd6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8e7cbd697f51119acd81fc0ccace8e668f1be141))
* **deps:** update dependency @sentry/node to v7.111.0 ([ca20635](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/ca20635faa54c5dceda059be3df57887ccfc9b7a))
* **deps:** update dependency @sentry/node to v7.112.1 ([8b7b97c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8b7b97c14d3f70d879a240c6d24207d395b00d49))
* **deps:** update dependency @toegang-voor-iedereen/kimi-baseworker to v3.9.2 ([9ae5631](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/9ae56317fd2743da3a979355e9126979579f2b9d))
* **deps:** update dependency zod to v3.22.5 ([218466e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/218466ed07712c9937e6294b43fec132119fc2d1))
* **deps:** update dependency zod to v3.23.0 ([4d858e2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/4d858e204a31fb277fd62b608c560f0626a8bf8a))
* **deps:** update dependency zod to v3.23.3 ([6093b43](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/6093b431421aebcc1fcd3ad08fc67c34fafb2efb))


### Features

* apply bold label ([8cae5ec](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8cae5ec44a982ee94d970f9329d7501b8c416e44))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/v1.1.0...v1.2.0) (2024-04-08)


### Bug Fixes

* **deps:** update dependency @sentry/node to v7.109.0 ([f337cfb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/f337cfb1fd6682d00f279bffaaab33a4de639405))


### Features

* format basic classified content at word level ([8f5a687](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/8f5a687fce65017ce9565c4bd28384fa4734b387))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/compare/v1.0.0...v1.1.0) (2024-04-04)


### Bug Fixes

* build ([50f5263](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/50f526360b96f1a00624f3a4019495cab58c175c))


### Features

* rename .eslintrc ([7f47028](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/7f47028bd4cfb8c41f81319e7c2d6487eeb436fb))
* update to node 20 ([b56a254](https://gitlab.com/toegang-voor-iedereen/pdf/workers/convert-document-to-output-html/commit/b56a254ec077142dd8d1e242bb9254ddc18562bd))
