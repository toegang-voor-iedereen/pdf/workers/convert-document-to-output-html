// @ts-check
import baseConfig from '@toegang-voor-iedereen/configs/eslint.config.mjs'
import tseslint from 'typescript-eslint'

export default tseslint.config(...baseConfig)
